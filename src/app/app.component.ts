import { Component, OnInit, ViewContainerRef, ApplicationRef } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';
import { SpinnerService } from './@core/spinner/spinner.service';
import { ToastsManager } from 'ng2-toastr';
import { TranslateService } from '@ngx-translate/core';
import { CurrentUserService } from './@core/auth/current-user.service';
import { MlLoader } from './@core/ml/ml.loader';
import { Subscription } from 'rxjs/Subscription';
import { MlMissingHandler } from './@core/ml/ml.missing.handler';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {

  title = 'Vehicle Log Book';
  showPageLoader: boolean;

  private LabelsLoadingSubscr: Subscription;

  constructor(private spinnerService: SpinnerService,
    private router: Router,
    public toastr: ToastsManager,
    vRef: ViewContainerRef,
    public translate: TranslateService,
    private cus: CurrentUserService,
    applicationRef: ApplicationRef) {

    this.toastr.setRootViewContainerRef(vRef);
    // for ng2-bootstrap-modal in angualar 5
    Object.defineProperty(applicationRef, '_rootComponents', { get: () => applicationRef['components'] });
    // --- Subscribe for labels loaded event to reload
    // --- translation becouse late load fronm db
    translate.addLangs(['en', 'bg']);
    translate.setDefaultLang('en');
    const currLoader: MlLoader = translate.currentLoader as MlLoader;
    this.LabelsLoadingSubscr = currLoader.LabelsLoading
      .subscribe((isLoading: boolean) => {
        console.log('Labels Loading: ' + isLoading);
        const lang = this.translate.currentLang;
        const mh: MlMissingHandler = this.translate.missingTranslationHandler as MlMissingHandler;
        mh.isLabelsLoading = isLoading;
        if (!isLoading) {
          // this.translate.setTranslation(lang, this.translate.currentLoader.getTranslation(lang), false);
          this.translate.reloadLang(lang);
        }
      });
    const browserLang = translate.getBrowserLang();
    // console.log('browserLang: ' + browserLang);
    // translate.use(browserLang.match(/en|bg/) ? browserLang : 'en');
    translate.use(this.cus.language);

  }

  ngOnInit() {
    this.spinnerService.status.subscribe(
      (val: boolean) => {
        this.showPageLoader = val;
        this.router.events.filter(event => event instanceof NavigationEnd).subscribe((e: NavigationEnd) => {
          // this.spinnerService.
          window.scrollTo(0, 0);
        });
      });
    /*
  this.router.events.subscribe((evt) => {
    if (!(evt instanceof NavigationEnd)) {
      return;
    }
  });*/
  }
}
