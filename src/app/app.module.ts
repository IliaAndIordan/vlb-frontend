import { NgModule, Injector } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { LocationStrategy, HashLocationStrategy } from '@angular/common';

import { ChartsModule } from 'ng2-charts';
import { HttpModule, Http } from '@angular/http';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { TranslateModule, TranslateLoader, TranslateService, TranslatePipe, MissingTranslationHandler } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';


import { AppComponent } from './app.component';
// Routing
import { AppRoutingModule, routableComponents } from './app-routing.module';
// Modules
// import { JwtModule, JWT_OPTIONS } from '@auth0/angular-jwt';
import { ToastModule, ToastOptions } from 'ng2-toastr/ng2-toastr';
// Languages
import { LOCALE_ID } from '@angular/core';
import { registerLocaleData } from '@angular/common';
import localeGB from '@angular/common/locales/en-GB';
import localeBG from '@angular/common/locales/bg';
import localeDE from '@angular/common/locales/en-DE';
import localeUS from '@angular/common/locales/es-US';
// ML
import { MlLoader } from './@core/ml/ml.loader';
import { MlService } from './@core/ml/api/ml.service';
import { MlMissingHandler } from './@core/ml/ml.missing.handler';

// Feature Modules
import { CoreModule } from './@core/core.module';
import { SharedModule } from './@shared/shared.module';
import { HomeModule } from './home/home.module';
import { VehiclesModule } from './vehicles/vehicles.module';


// Services
import { CurrentUserService } from './@core/auth/current-user.service';
// import { TokenService } from './@core/auth/token.service';
// import { TokenFactory } from './@core/auth/token-factory';

//  Components
import { CarouselModule } from 'ngx-bootstrap/carousel/carousel.module';

registerLocaleData(localeGB, 'en');
registerLocaleData(localeBG, 'bg');
registerLocaleData(localeDE, 'us');
registerLocaleData(localeDE, 'de');

export class VlbToastOptions extends ToastOptions {
  // animate = 'flyRight'; // you can override any options available
  newestOnTop = false;
  showCloseButton = true;
  positionClass = 'toast-top-center';
}


@NgModule({
  imports: [
    SharedModule,
    CoreModule,
    BrowserAnimationsModule,
    HttpModule,
    AppRoutingModule,
    ChartsModule,
    HomeModule,
    VehiclesModule,
    CarouselModule.forRoot(),
    ToastModule.forRoot(),
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useClass: MlLoader,
        deps: [Http, CurrentUserService, MlService]
      },
      missingTranslationHandler: {
        provide: MissingTranslationHandler,
        useClass: MlMissingHandler,
        deps: [MlService]
      },
      useDefaultLang: false

    }),
  ],
  declarations: [
    AppComponent,
    routableComponents
  ],
  exports: [
    TranslateModule,
  ],
  providers: [
    TranslateService,
    {provide: ToastOptions, useClass: VlbToastOptions},
    { provide: LocationStrategy, useClass: HashLocationStrategy },
    { provide: LOCALE_ID, useValue: 'en' }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
  /**
     * Allows for retrieving singletons using `AppModule.injector.get(MyService)`
     * This is good to prevent injecting the service as constructor parameter.
     */
  static injector: Injector;
  constructor(injector: Injector) {
    AppModule.injector = injector;
  }

}
