import { NgModule } from '@angular/core';

// Modules
import { CoreModule } from '../@core/core.module';
import { SharedModule } from '../@shared/shared.module';
import { VehiclesRoutingModule, RoutedComponents } from './routing.module';
// Components
import { VehicleDetailsTabsComponent } from './tabs/vehicle-details-tabs.component';
import { VehiclePurchaseDateValidator } from './validators/vehicle-purchase-date.validator';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
// Dialogs
import { EditExpenseComponent } from './modals/edit-expense.component';
import { EditGasComponent } from './modals/edit-gas.component';
import { EditVehicleComponent } from './modals/edit-vehicle.component';


@NgModule({
  imports: [
    VehiclesRoutingModule,
    SharedModule,
    CoreModule,
    NgbModule
  ],
  declarations: [
    RoutedComponents,
    VehicleDetailsTabsComponent,
    EditVehicleComponent,
    EditExpenseComponent,
    EditGasComponent,
  ],
  exports: [
    VehicleDetailsTabsComponent,
  ],
  entryComponents: [
    EditVehicleComponent,
    EditExpenseComponent,
    EditGasComponent

  ]
})
export class VehiclesModule { }
