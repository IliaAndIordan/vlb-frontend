import { Validators } from '@angular/forms';
import { Validator } from '../../@core/validation/validator';

export class VehicleTrackInMlValidator extends Validator {

    private _controlName: string = 'track_in_mi';
    private _rules: Array<Validators> = [];

    private _messageTemplates: Object = {
        // maxlength: this.validationMessageService.maxLength(this._controlName, this._maxLength)
    };

    public get controlName(): string {
        return this._controlName;
    }

    public get messageTemplates(): Object {
        return this._messageTemplates;
    }

    public get rules(): Array<Validators> {
        return this._rules;
    }
}
