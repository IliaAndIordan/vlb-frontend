import { Validators } from '@angular/forms';
import { Validator } from '../../@core/validation/validator';

export class ExpenseDateValidator extends Validator {

    private _controlName: string = 'exp_date';
    private _rules: Array<Validators> = [Validators.required];

    private _messageTemplates: Object = {
        required: this.validationMessageService.required(this._controlName),
        // maxlength: this.validationMessageService.maxLength(this._controlName, this._maxLength)
    };

    public get controlName(): string {
        return this._controlName;
    }

    public get messageTemplates(): Object {
        return this._messageTemplates;
    }

    public get rules(): Array<Validators> {
        return this._rules;
    }
}
