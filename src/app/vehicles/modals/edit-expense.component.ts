import { Component, trigger, state, style, transition, animate, Input, OnInit, Output, EventEmitter, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators, AbstractControl, ValidatorFn, FormArray } from '@angular/forms';
import { DialogComponent, DialogService } from 'ng2-bootstrap-modal';
import { getLocaleCurrencySymbol } from '@angular/common';

import { ToastsManager } from 'ng2-toastr';
import { NgbInputDatepicker } from '@ng-bootstrap/ng-bootstrap';
import { NgbDate } from '@ng-bootstrap/ng-bootstrap/datepicker/ngb-date';
// Services
import { ValidationMessageService } from '../../@core/validation/validation-message.service';
// Validators
import { ExpenseDateValidator } from '../validators/expense-date.validator';
import { ExpenseMilageKmValidator } from '../validators/expense-milage-km.validator';
import { ExpensePriceValidator } from '../validators/expense-price.validator';
import { ExpenseNoteValidator } from '../validators/expense-note.validator';
// Models

export interface IEditExpenceModel {
  exp_id: number;
  vehicle_id: number;
  exp_date: Date;

  exp_millage: number;
  exp_note: string;
  exp_price: number;
  currency_lbl: string;
  track_id: number;
}

@Component({
  selector: 'app-edit-expense',
  templateUrl: './edit-expense.component.html',
})
export class EditExpenseComponent extends DialogComponent<IEditExpenceModel, IEditExpenceModel> implements OnInit {

  constructor(dialogService: DialogService,
    private fb: FormBuilder,
    private vms: ValidationMessageService,
    private toastr: ToastsManager) { super(dialogService); }

  @ViewChild('d') expenseDatePicker: NgbInputDatepicker;

  /**
   * Fields
   */
  public exp_id: number;
  public vehicle_id: number;
  public exp_date: Date;

  public exp_millage: number;
  public exp_note: string;
  public exp_price: number;
  public currency_lbl: string;
  public track_id: number;

  /**
   * Form
   */
  editExpenseForm: FormGroup;
  expDateValidator = new ExpenseDateValidator(this.vms);
  expMilageValidator = new ExpenseMilageKmValidator(this.vms);
  expPriceValidator = new ExpensePriceValidator(this.vms);
  expNoteValidator = new ExpenseNoteValidator(this.vms);

  // Display Fields
  public expenseDateDisplay: NgbDate;

  ngOnInit() {
    // get Date Fields Values
    this.expenseDateDisplay = this.getExpenseDateDisplay();
    // Init form ffields
    this.editExpenseForm = this.fb.group({
      exp_date: [this.exp_date, this.expDateValidator.rules],
      exp_millage: [this.exp_millage, this.expMilageValidator.rules],
      exp_price: [this.exp_price, this.expPriceValidator.rules],
      exp_note: [this.exp_note, this.expNoteValidator.rules],
    });
    // Subscribe for Date Controll Events
    const expenseDateControl = this.editExpenseForm.get(this.expDateValidator.controlName);
    expenseDateControl.valueChanges
      .subscribe(value => {
        this.onSelectExpenseDate(value);
      });

  }

  get title(): string {
    let rv = 'Add Expense';
    if (this.exp_id) {
      rv = 'Edit Expense';
    }
    return rv;
  }

  /*
  private formDirty(): boolean {
    return this.editVehicleForm.get(this.nameValidator.controlName).dirty ||
    this.editVehicleForm.get(this.modelValidator.controlName).dirty;
  }
  */

  onConfirm(): void {
    this.result = this.editExpenseForm.value;
    this.close();
  }

  onCancel(): void {
    this.result = null;
    this.close();
  }

  public submit(): void {
    console.log('submit');
    // SUBMI Tdata.
    this.result = this.editExpenseForm.value;
    this.result.exp_date = this.exp_date;
    this.result.exp_id = this.exp_id;
    this.result.vehicle_id = this.vehicle_id;
    this.result.currency_lbl = this.currency_lbl;
    this.result.track_id = this.track_id;
    console.dir(this.result);
    this.close();
  }

  //#region Date Fields Helpers

  private getExpenseDateDisplay(): NgbDate {
    let dateValue = new Date();
    if (this.exp_date) {
      dateValue = new Date(this.exp_date);
    }

    return new NgbDate(dateValue.getFullYear(), dateValue.getMonth() + 1, dateValue.getDate());
  }


  private onSelectExpenseDate(ngbDate: NgbDate) {
    // console.log('onSelectPurchaseDate: e:', ngbDate);
    if (ngbDate) {
      // If not a valid date the type would be a string.
      const newDate = new Date(ngbDate.year, ngbDate.month - 1, ngbDate.day);
      this.exp_date = newDate;
      this.expenseDateDisplay = this.getExpenseDateDisplay();
      // this.spinnerService.display(true);
      // this.getUserList();
    } else {
      this.expenseDateDisplay = this.getExpenseDateDisplay();
    }
  }

  //#endregion

  //#region Price field helpers
  public get currencySymbol(): string {
    const locale = this.currency_lbl.replace('_', '-');
    // const name = getLocaleCurrencyName(locale);
    const symbol = getLocaleCurrencySymbol(locale);

    return symbol;
  }

  public get expPriceDisplay(): string {
    const locale = this.currency_lbl.replace('_', '-');
    return new Intl.NumberFormat(locale, { style: 'decimal' , useGrouping: true, minimumFractionDigits: 2 }).format(this.exp_price);
  }


  //#endregion

}
