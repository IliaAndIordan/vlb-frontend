import { Component, trigger, state, style, transition, animate, Input, OnInit, Output, EventEmitter, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators, AbstractControl, ValidatorFn, FormArray } from '@angular/forms';
import { DialogComponent, DialogService } from 'ng2-bootstrap-modal';
import { ToastsManager } from 'ng2-toastr';
import { NgbInputDatepicker } from '@ng-bootstrap/ng-bootstrap';
import { NgbDate } from '@ng-bootstrap/ng-bootstrap/datepicker/ngb-date';
// Services
import { ValidationMessageService } from '../../@core/validation/validation-message.service';
// Models
import { VehicleModel, DateModel } from '../../@core/api/vehicle/dto';
// Validators
import { VehicleModelValidator } from '../validators/vehicle-model.validator';
import { VehicleNameValidator } from '../validators/vehicle-name.validator';
import { VehiclePurchaseDateValidator } from '../validators/vehicle-purchase-date.validator';
import { VehicleFirstRegistrationDateValidator } from '../validators/vehicle-first-registration-date.validator';
import { VehicleSaleDateValidator } from '../validators/vehicle-sale-date.validator';
import { VehicleMilageKmStartValidator } from '../validators/vehicle-milage-km-start.validator';
import { VehicleMilageKmValidator } from '../validators/vehicle-milage-km.validator';
import { VehicleTrackInMlValidator } from '../validators/vehicle-track_in_ml.validator';

export interface IEditVehicleModel {
  name: string;
  model: string;
  purchase_date: DateModel;
  first_reg: DateModel;
  sale_date: DateModel;
  milage_km_start: number;
  milage_km_current: number;
  track_in_mi: boolean;
}


@Component({
  selector: 'app-edit-vehicle',
  templateUrl: './edit-vehicle.component.html',
})
export class EditVehicleComponent extends DialogComponent<IEditVehicleModel, IEditVehicleModel> implements OnInit {

  constructor(dialogService: DialogService,
    private fb: FormBuilder,
    private vms: ValidationMessageService,
    private toastr: ToastsManager) { super(dialogService); }

  @ViewChild('pd') purchaseDatePicker: NgbInputDatepicker;
  @ViewChild('fr') firstRegDatePicker: NgbInputDatepicker;
  @ViewChild('sd') saleDatePicker: NgbInputDatepicker;
  @ViewChild('input') trackInMlInput;

  // tslint:disable-next-line:no-output-on-prefix
  @Output() onChecked: EventEmitter<boolean> = new EventEmitter<boolean>();

  /**
   * Fields
   */
  public name: string;
  public model: string;
  public purchase_date: DateModel;
  public first_reg: DateModel;
  public sale_date: DateModel;
  public milage_km_start: number;
  public milage_km_current: number;
  public track_in_mi: boolean;

  // Display Fields
  public purchaseDateDisplay: NgbDate;
  public firstRegDateDisplay: NgbDate;
  public saleDateDisplay: NgbDate;


  /*

    public image: string;

    public currency_lbl: string;
    */

  /**
   * Form
   */
  editVehicleForm: FormGroup;
  nameValidator = new VehicleNameValidator(this.vms);
  modelValidator = new VehicleModelValidator(this.vms);
  purchaseDateValidator = new VehiclePurchaseDateValidator(this.vms);
  firstRegDateValidator = new VehicleFirstRegistrationDateValidator(this.vms);
  saleDateValidator = new VehicleSaleDateValidator(this.vms);
  milageKmStartValidator = new VehicleMilageKmStartValidator(this.vms);
  milageKmValidator = new VehicleMilageKmValidator(this.vms);
  trackInMlValidator = new VehicleTrackInMlValidator(this.vms);

  ngOnInit() {
    // get Date Fields Values
    this.purchaseDateDisplay = this.getPurchaseDateDisplay();
    this.firstRegDateDisplay = this.getFirstRegDateDisplay();
    this.saleDateDisplay = this.getSaleDateDisplay();

    // Init form ffields
    this.editVehicleForm = this.fb.group({
      name: [this.name, this.nameValidator.rules],
      model: [this.model, this.modelValidator.rules],
      purchase_date: [this.purchase_date ? this.purchase_date.date : undefined, this.purchaseDateValidator.rules],
      first_reg: [this.first_reg ? this.first_reg.date : undefined, this.firstRegDateValidator.rules],
      sale_date: [this.sale_date ? this.sale_date.date : undefined, this.saleDateValidator.rules],
      milage_km_start: [this.milage_km_start, this.milageKmStartValidator.rules],
      milage_km_current: [this.milage_km_current, this.milageKmValidator.rules],
      track_in_mi: [this.track_in_mi, this.trackInMlValidator.rules],
    });

    // Subscribe for Date Controll Events
    const purchaseDateControl = this.editVehicleForm.get(this.purchaseDateValidator.controlName);
    purchaseDateControl.valueChanges
      .subscribe(value => {
        this.onSelectPurchaseDate(value);
      });

    const firstRegDateControl = this.editVehicleForm.get(this.firstRegDateValidator.controlName);
    firstRegDateControl.valueChanges
      .subscribe(value => {
        this.onSelectFirstRegDate(value);
      });

    const saleDateControl = this.editVehicleForm.get(this.saleDateValidator.controlName);
    saleDateControl.valueChanges
      .subscribe(value => {
        this.onSelectSaleate(value);
      });

  }

  get title(): string {
    let rv = 'Create/Edit';
    if (this.model) {
      rv = 'Edit ' + this.model;
    }
    return rv;
  }

  /*
  private formDirty(): boolean {
    return this.editVehicleForm.get(this.nameValidator.controlName).dirty ||
    this.editVehicleForm.get(this.modelValidator.controlName).dirty;
  }
  */

  onConfirm(): void {
    this.result = this.editVehicleForm.value;
    this.close();
  }

  onCancel(): void {
    this.result = null;
    this.close();
  }

  public submit(): void {
    console.log('submit');
    // SUBMI Tdata.
    this.result = this.editVehicleForm.value;
    this.result.purchase_date = this.purchase_date;
    this.result.first_reg = this.first_reg;
    this.result.sale_date = this.sale_date;
    this.result.track_in_mi = this.track_in_mi;
    console.dir(this.result);
    this.close();
  }

  handleCheck(checked: boolean) {
    // What we are sending to the event handler that is being passed to parent function.
    this.track_in_mi = checked;
    this.onChecked.emit(checked);
    this.editVehicleForm.updateValueAndValidity();
  }

  public get labelOn(): string {
    return 'ml';
  }
  public get labelOff(): string {
    return 'km';
  }

  //#region Date Fields Helpers

  private getPurchaseDateDisplay(): NgbDate {
    let dateValue = new Date();
    if (this.purchase_date && this.purchase_date.date) {
      dateValue = this.purchase_date.date;
    }

    return new NgbDate(dateValue.getFullYear(), dateValue.getMonth() + 1, dateValue.getDate());
  }


  private onSelectPurchaseDate(ngbDate: NgbDate) {
    // console.log('onSelectPurchaseDate: e:', ngbDate);
    if (ngbDate) {
      // If not a valid date the type would be a string.
      const newDate = new Date(ngbDate.year, ngbDate.month - 1, ngbDate.day);
      this.purchase_date.date = newDate;
      this.purchaseDateDisplay = this.getPurchaseDateDisplay();
      // this.spinnerService.display(true);
      // this.getUserList();
    } else {
      this.purchaseDateDisplay = this.getPurchaseDateDisplay();
    }
  }

  private getFirstRegDateDisplay(): NgbDate {
    let dateValue = new Date();
    if (this.first_reg && this.first_reg.date) {
      dateValue = this.first_reg.date;
    }

    return new NgbDate(dateValue.getFullYear(), dateValue.getMonth() + 1, dateValue.getDate());
  }


  private onSelectFirstRegDate(ngbDate: NgbDate) {
    // console.log('onSelectFirstRegDate: e:', ngbDate);
    if (ngbDate) {
      // If not a valid date the type would be a string.
      const newDate = new Date(ngbDate.year, ngbDate.month - 1, ngbDate.day);
      this.first_reg.date = newDate;
      this.firstRegDateDisplay = this.getFirstRegDateDisplay();
      // this.spinnerService.display(true);
      // this.getUserList();
    } else {
      this.firstRegDateDisplay = this.getFirstRegDateDisplay();
    }
  }

  private getSaleDateDisplay(): NgbDate {
    let dateValue;
    if (this.sale_date && this.sale_date.date) {
      dateValue = this.sale_date.date;
    }
    if (dateValue) {
      return new NgbDate(dateValue.getFullYear(), dateValue.getMonth() + 1, dateValue.getDate());
    }
    return undefined;

  }


  private onSelectSaleate(ngbDate: NgbDate) {
    // console.log('onSelectFirstRegDate: e:', ngbDate);
    if (ngbDate) {
      // If not a valid date the type would be a string.
      const newDate = new Date(ngbDate.year, ngbDate.month - 1, ngbDate.day);
      this.sale_date.date = newDate;
      this.saleDateDisplay = this.getSaleDateDisplay();

    } else {
      this.saleDateDisplay = this.getSaleDateDisplay();
    }
  }

  //#endregion
}
