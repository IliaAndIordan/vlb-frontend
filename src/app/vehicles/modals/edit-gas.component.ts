import { Component, trigger, state, style, transition, animate, Input, OnInit, Output, EventEmitter, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators, AbstractControl, ValidatorFn, FormArray } from '@angular/forms';
import { DialogComponent, DialogService } from 'ng2-bootstrap-modal';
import { getLocaleCurrencySymbol } from '@angular/common';

import { ToastsManager } from 'ng2-toastr';
import { NgbInputDatepicker } from '@ng-bootstrap/ng-bootstrap';
import { NgbDate } from '@ng-bootstrap/ng-bootstrap/datepicker/ngb-date';
// Services
import { ValidationMessageService } from '../../@core/validation/validation-message.service';
// Validators
import { GasDateValidator } from '../validators/gas-date.validator';
import { GasMilageKmValidator } from '../validators/gas-milage-km.validator';
import { GasPriceValidator } from '../validators/gas-price.validator';
import { GasQuantityValidator } from '../validators/gas-quantity.validator';
import { GasService } from '../../@core/api/gas/gas.service';
import { ResponseVehicleGasLoad, VehicleGasModel, IVehicleGasModel, FuelRetailer } from '../../@core/api/gas/dto';
import { CurrentUserService } from '../../@core/auth/current-user.service';
import { GasFuelEconomyValidator } from '../validators/gas-fuel-economy.validator';
// Models

export interface IEditGasModel {
  gas_id: number;
  vehicle_id: number;
  gas_date: Date;

  gas_millage: number;
  gas_quantity: number;
  gas_total_price: number;
  currency_lbl: string;
  gas_location_id: number;
  location_brand: string;

  cal_fule_consumption: number;
  cal_price_per_l: number;
  // From Statistics
  avg_gas_price: number;
  fule_economy: number;
}

@Component({
  selector: 'app-edit-gas',
  templateUrl: './edit-gas.component.html',
})
export class EditGasComponent extends DialogComponent<IEditGasModel, ResponseVehicleGasLoad> implements OnInit {

  constructor(dialogService: DialogService,
    private fb: FormBuilder,
    private vms: ValidationMessageService,
    private toastr: ToastsManager,
    private gasService: GasService,
    private cus: CurrentUserService) { super(dialogService); }

  @ViewChild('d') expenseDatePicker: NgbInputDatepicker;

  // tslint:disable-next-line:no-output-on-prefix
  @Output() onHover: EventEmitter<any> = new EventEmitter<any>();
  @Output() offHover: EventEmitter<any> = new EventEmitter<any>();
  /**
   * Fields
   */
  public gas_id: number;
  public vehicle_id: number;
  public gas_date: Date;

  public gas_millage: number;
  public gas_quantity: number;
  public gas_total_price: number;
  public currency_lbl: string;
  public gas_location_id: number;
  public location_brand: string;

  public cal_fule_consumption: number;
  public cal_price_per_l: number;
  // From Statistics
  public avg_gas_price: number;
  public fule_economy: number;

  public fuelRetailers: FuelRetailer[];


  /**
   * Form
   */
  editGasForm: FormGroup;
  gasDateValidator = new GasDateValidator(this.vms);
  gasMilageValidator = new GasMilageKmValidator(this.vms);
  gasPriceValidator = new GasPriceValidator(this.vms);
  gasQuantityValidator = new GasQuantityValidator(this.vms);
  gasFuelEconomyValidator = new GasFuelEconomyValidator(this.vms);

  // Display Fields
  public gasDateDisplay: NgbDate;

  ngOnInit() {
    // get Date Fields Values
    this.gasDateDisplay = this.getGasDateDisplay();
    this.fuelRetailers = FuelRetailer.getFuelRetailers();
    // Init form ffields
    this.editGasForm = this.fb.group({
      gas_date: [this.gas_date, this.gasDateValidator.rules],
      gas_millage: [this.gas_millage, this.gasMilageValidator.rules],
      gas_total_price: [this.gas_total_price, this.gasPriceValidator.rules],
      gas_quantity: [this.gas_quantity, this.gasQuantityValidator.rules],
      cal_fule_consumption: [this.cal_fule_consumption, this.gasFuelEconomyValidator.rules],
    });
    // Subscribe for Date Controll Events
    const gasDateControl = this.editGasForm.get(this.gasDateValidator.controlName);
    gasDateControl.valueChanges
      .subscribe(value => {
        this.onSelectGasDate(value);
      });

    const quantityContraol = this.editGasForm.get(this.gasQuantityValidator.controlName);
    quantityContraol.valueChanges
      .subscribe(value => {
        this.gas_quantity = value;
        this.updateValues();
      });

      const priceContraol = this.editGasForm.get(this.gasPriceValidator.controlName);
      priceContraol.valueChanges
        .subscribe(value => {
          this.gas_total_price = value;
          this.updateValues();
        });

    this.cal_price_per_l = 0;
    this.updateValues();

  }

  get title(): string {
    let rv = 'Add Gas';
    if (this.gas_id) {
      rv = 'Edit Gas';
    }
    return rv;
  }

  get brand(): FuelRetailer {
    return new FuelRetailer(this.location_brand);
  }

  private updateValues(): void {
    if (this.gas_quantity && this.gas_quantity > 0) {
      this.cal_price_per_l = this.gas_total_price / this.gas_quantity;
    }
    console.log('updateValues: ' + this.gas_quantity + ' cal_price_per_l=' + this.cal_price_per_l);
  }

  public clicBrandOption(item: FuelRetailer): void {
    this.location_brand = item.name;
  }

  public get gasMeasure(): string {
    let retValue = 'L';

    if (this.cus.selectedVehicle && this.cus.selectedVehicle.track_in_mi) {
      retValue = 'gl';
    }
    return retValue;
  }

  public get gasFuelEconomyMeasure(): string {
    let retValue = 'L/100 km';

    if (this.cus.selectedVehicle && this.cus.selectedVehicle.track_in_mi) {
      retValue = 'MPG';
    }
    return retValue;
  }



  /*
  private formDirty(): boolean {
    return this.editVehicleForm.get(this.nameValidator.controlName).dirty ||
    this.editVehicleForm.get(this.modelValidator.controlName).dirty;
  }
  */

  onConfirm(): void {
    this.result = this.editGasForm.value;
    this.close();
  }

  onCancel(): void {
    this.result = null;
    this.close();
  }

  public submit(): void {
    console.log('submit');
    // SUBMI Tdata.
    const vgmodel: VehicleGasModel = this.editGasForm.value; // new VehicleGasModel();
    vgmodel.gas_id = this.gas_id;
    vgmodel.vehicle_id = this.vehicle_id;
    vgmodel.gas_date = this.gas_date;
    // vgmodel.gas_millage = this.gas_millage;
    // vgmodel.gas_quantity = this.gas_quantity;
    // vgmodel.gas_total_price = this.gas_total_price;
    vgmodel.currency_lbl = this.currency_lbl;
    vgmodel.gas_location_id = this.gas_location_id;

    vgmodel.location_brand = this.location_brand;
    // vgmodel.cal_fule_consumption = this.cal_fule_consumption;
    vgmodel.cal_price_per_l = this.cal_price_per_l;
    vgmodel.avg_gas_price = this.avg_gas_price;
    vgmodel.fule_economy = this.fule_economy;

    if (vgmodel.gas_id && vgmodel.gas_id > 0) {
      this.gasService.updateGas(vgmodel)
        .subscribe(
          (result: ResponseVehicleGasLoad) => this.processGas(result),
          error => {
            this.toastr.error(error);
          });
    } else {
      this.gasService.addGas(vgmodel)
        .subscribe(
          (result: ResponseVehicleGasLoad) => this.processGas(result),
          error => {
            this.toastr.error(error);
          });
    }


  }

  private processGas(result: ResponseVehicleGasLoad): void {
    // this.spinnerService.display(false);
    this.result = result;
    this.close();
  }

  //#region Date Fields Helpers

  private getGasDateDisplay(): NgbDate {
    let dateValue = new Date();
    if (this.gas_date) {
      dateValue = new Date(this.gas_date);
    }

    return new NgbDate(dateValue.getFullYear(), dateValue.getMonth() + 1, dateValue.getDate());
  }


  private onSelectGasDate(ngbDate: NgbDate) {
    // console.log('onSelectPurchaseDate: e:', ngbDate);
    if (ngbDate) {
      // If not a valid date the type would be a string.
      const newDate = new Date(ngbDate.year, ngbDate.month - 1, ngbDate.day, new Date().getHours(), new Date().getMinutes() );
      // newDate = new Date(newDate.getTime() + (new Date().getTimezoneOffset() * 60000));
      this.gas_date = newDate;
      this.gasDateDisplay = this.getGasDateDisplay();
      // this.spinnerService.display(true);
      // this.getUserList();
    } else {
      this.gasDateDisplay = this.getGasDateDisplay();
    }
  }

  //#endregion

  //#region Price field helpers
  public get currencySymbol(): string {
    const locale = this.currency_lbl.replace('_', '-');
    // const name = getLocaleCurrencyName(locale);
    const symbol = getLocaleCurrencySymbol(locale);

    return symbol;
  }

  public get gasPriceDisplay(): string {
    const locale = this.currency_lbl.replace('_', '-');
    return new Intl.NumberFormat(locale, { style: 'decimal', useGrouping: true, minimumFractionDigits: 2 }).format(this.gas_total_price);
  }


  //#endregion


  getTooltip(key: string): string {
    return VehicleGasTooltip[key];
  }
}

export const VehicleGasTooltip = {
  cal_price_per_l: 'Callculated price per litter',
  gas_id: 'Unique Identifier of this record'
};

