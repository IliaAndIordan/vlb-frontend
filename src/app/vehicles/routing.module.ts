import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AutGuard } from '../@core/guards/aut-guard.service';
import { AppRoutes } from '../@core/const/routes/app-routes.const';
// Components
import { HomeComponent } from './home.component';
import { VehiclesComponent } from './vehicles.component';
import { VehicleDetailsComponent } from './details/details.component';

const routes: Routes = [
  {
    path: AppRoutes.Vehicles, component: VehiclesComponent, canActivate: [AutGuard],
    data: { title: 'Vehicles', icon: 'icon-info', group: 'Vehicles', divider: false },
    children: [
      {
        path: '', component: HomeComponent
      },
      { path: ':id', component: VehicleDetailsComponent,
        data: { title: 'Details', icon: 'icon-info', group: 'Vehicles', divider: false }}
    ]
  },
  /*{ path: AppRoutes.Vehicle, component: VehiclesComponent, canActivate: [AutGuard],
    data: { title: 'Vehicle', icon: 'icon-info', group: 'Vehicle', divider: false },
    children: [
      { path: '', component: HomeComponent  },
    ]
  }*/
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class VehiclesRoutingModule { }
export const RoutedComponents = [VehiclesComponent, HomeComponent, VehicleDetailsComponent];
