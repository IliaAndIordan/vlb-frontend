import { Component, OnDestroy, OnInit, Input, Output, OnChanges, ViewContainerRef } from '@angular/core';
import { FormBuilder, FormGroup, Validators, AbstractControl, ValidatorFn } from '@angular/forms';
import { EventEmitter, SimpleChange, ViewChild } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ToastsManager } from 'ng2-toastr';
import { DialogService } from 'ng2-bootstrap-modal';
// Table
import { DatatableComponent } from '@swimlane/ngx-datatable';
// Services
import { CurrentUserService } from '../../@core/auth/current-user.service';
import { GasService } from '../../@core/api/gas/gas.service';
import { VehicleService } from '../../@core/api/vehicle/vehicle.service';
import { ExpenseService } from '../../@core/api/expenses/expense.service';

// Models
import { VehicleModel } from '../../@core/api/vehicle/dto';
import { ResponseVehicleGasLoad, IVehicleGasModel, VehicleGasModel } from '../../@core/api/gas/dto';
import { VehicleExpenceModel, ResponseVehicleExpences, IVehicleExpenceModel } from '../../@core/api/expenses/dto';

// Dialog
import { EditVehicleComponent } from '../modals/edit-vehicle.component';
import { EditExpenseComponent } from '../modals/edit-expense.component';
import { EditGasComponent, IEditGasModel } from '../modals/edit-gas.component';
import { GasBrand } from '../../@core/const/gas-brand';



@Component({
  selector: 'app-vehicle-details-tabs',
  templateUrl: './vehicle-details-tabs.component.html',
  styleUrls: ['./vehicle-details-tabs.component.css']
})
export class VehicleDetailsTabsComponent implements OnInit {

  private expences: VehicleExpenceModel[];
  public rows: VehicleExpenceModel[];

  private gasLoad: VehicleGasModel[];
  public gasRows: VehicleGasModel[];




  @ViewChild(DatatableComponent) table: DatatableComponent;
  @ViewChild(DatatableComponent) tableGas: DatatableComponent;

  constructor(private toastr: ToastsManager,
    private cus: CurrentUserService,
    private vehicleService: VehicleService,
    private expenseService: ExpenseService,
    private gasService: GasService,
    private modalService: NgbModal,
    private dialogService: DialogService) { }

  @Input() vehicle: VehicleModel;

  ngOnInit() {


  }


  tabChange(e) {
    console.log('tabChange');
    console.dir(e);
    if (e.nextId === 'tab_expences') {
      if (!this.expences || this.expences.length === 0) {
        this.getVehicleExpences(this.vehicle.vehicleId);
      }

    } else if (e.nextId === 'tab_gas') {
      if (!this.gasLoad || this.gasLoad.length === 0) {
        this.getVehicleGasLoad(this.vehicle.vehicleId);
      }
    }
    /*
    if (e.nextId === 'distributor-pricing' && !this.distTabLoaded) {
      this.distTabSpinner = true;
      this.distributorPricingTabClick.emit();
    }
    */
  }

  updateFilter(event) {
    const val = event.target.value.toLowerCase();

    // filter our data
    const temp = this.expences.filter(function (d) {
      return d.exp_note.toLowerCase().indexOf(val) !== -1 || !val;
    });

    // update the rows
    this.rows = temp;
    // Whenever the filter changes, always go back to the first page
    this.table.offset = 0;
  }

  //#region Gas Tab


  //#endregion


  getExpenseCellClass({ row, column, value }): any {

    let retValue = '';

    switch (column.prop) {
      case 'exp_millage':
      case 'exp_date':
      case 'exp_price':
        retValue = 'tdright';
        break;

      default:
        retValue = '';
        break;
    }
    return { 'tdright': retValue !== '' };
  }

  //#region  Data


  getVehicleExpences(vehicleId: number): void {
    // this.spinnerService.display(true);
    this.vehicleService.getVehicleExpences(vehicleId)
      .subscribe(
        (result: ResponseVehicleExpences) => this.processExpences(result),
        error => {
          this.toastr.error(error);
        });
  }

  private processExpences(result: ResponseVehicleExpences): void {
    // this.spinnerService.display(false);
    const data: IVehicleExpenceModel[] = result.data.vehicle_expences;
    this.expences = [];
    if (data) {
      data.forEach(element => {
        const expence: VehicleExpenceModel = VehicleExpenceModel.fromJSON(element);
        this.expences.push(expence);
      });

    }
    this.rows = this.expences;
    this.table.offset = 0;
    this.table.recalculate();
    console.dir(this.expences);
  }

  getVehicleGasLoad(vehicleId: number): void {
    // this.spinnerService.display(true);
    this.gasService.getVehicleGasLoad(vehicleId)
      .subscribe(
        (result: ResponseVehicleGasLoad) => {

          // this.spinnerService.display(false);
          const data: IVehicleGasModel[] = result.data.vehicle_gas_all;
          this.gasLoad = [];
          if (data) {
            data.forEach(element => {
              const gas: VehicleGasModel = VehicleGasModel.fromJSON(element);
              this.gasLoad.push(gas);
            });

          }
          this.gasRows = this.gasLoad;
          this.tableGas.offset = 0;
          this.tableGas.recalculate();

        },
        error => {
          this.toastr.error(error);
        });
  }

  private processGasLoad(result: ResponseVehicleGasLoad): void {
    // this.spinnerService.display(false);
    const data: IVehicleGasModel[] = result.data.vehicle_gas_all;
    this.gasLoad = [];
    if (data) {
      data.forEach(element => {
        const gas: VehicleGasModel = VehicleGasModel.fromJSON(element);
        this.gasLoad.push(gas);
      });

    }
    this.gasRows = this.gasLoad;
    this.tableGas.offset = 0;
    this.tableGas.recalculate();
  }

  //#endregion

  //#region Fields Values

  get timeInUseYmd(): any {
    return this.vehicle ? this.vehicle.getTimeInUseYmd() : undefined;
  }

  public get timeInUsePct(): number {
    let rv = 0;
    if (this.cus.vehiclesMaxTimeUsedMs() > 0 && this.vehicle) {
      rv = Math.ceil((this.vehicle.calcTimeInUseMs() / this.cus.vehiclesMaxTimeUsedMs()) * 100);
    }
    if (rv > 99) {
      rv = 99;
    }
    // console.log('timeInUsePct : ' + rv);
    return rv;
  }


  public progressbarType(pct: number, positive: boolean = true): string {
    let rv = 'success';
    if (pct === 100) {
      rv = positive ? 'info' : 'danger';
    } else if (pct > 50) {
      rv = positive ? 'success' : 'warning';
    } else if (pct > 20) {
      rv = positive ? 'warning' : 'success';
    } else {
      rv = positive ? 'danger' : 'info';
    }
    return rv;
  }

  //#endregion


  //#region  Modals

  //#region  Gas

  addGas(vehicleId: number): void {
    console.log('addGas-> vehicleId ' + vehicleId);
    const gas: VehicleGasModel = new VehicleGasModel();
    gas.vehicle_id = vehicleId;
    gas.currency_lbl = this.vehicle.currency_lbl;
    gas.gas_date = new Date();
    gas.gas_millage = this.vehicle.milage_km_current;
    gas.cal_fule_consumption = this.vehicle.vehicles_stat.fule_economy;
    gas.cal_price_per_l = this.vehicle.vehicles_stat.avg_gas_price;
    gas.gas_total_price = 0;
    gas.gas_quantity = 0;
    gas.location_brand = 'undefined';
    console.dir(gas);

    this.dialogService.addDialog(EditGasComponent, gas)
      .subscribe((res: ResponseVehicleGasLoad) => {
        if (res) {
          this.processGasLoad(res);
        }
      });
  }

  editGas(gas_id: number): void {
    console.log('editGas-> gas_id ' + gas_id);
    const gas: VehicleGasModel = this.getGasById(gas_id);
    if (gas) {
      if (!gas.cal_fule_consumption || gas.cal_fule_consumption === 0) {
        gas.cal_fule_consumption = this.vehicle.vehicles_stat.fule_economy;
      }
      this.dialogService.addDialog(EditGasComponent, gas)
        .subscribe((res: ResponseVehicleGasLoad) => {
          if (res) {
            this.processGasLoad(res);
          }
        });
    }
  }

  getGasById(gas_id: number): VehicleGasModel {
    let retValue: VehicleGasModel;

    if (this.gasLoad && this.gasLoad.length > 0) {
      retValue = this.gasLoad.find(x => x.gas_id === gas_id);
    }

    return retValue;
  }

  //#endregion

  //#region  Expenses

  addExpence(vehicleId: number): void {
    console.log('addExpence-> vehicleId ' + vehicleId);
    const exp: VehicleExpenceModel = new VehicleExpenceModel();
    exp.vehicle_id = vehicleId;
    exp.currency_lbl = this.vehicle.currency_lbl;
    exp.exp_date = new Date();
    exp.exp_millage = this.vehicle.milage_km_current;
    exp.exp_price = 0;
    exp.exp_note = '';
    console.dir(exp);

    this.dialogService.addDialog(EditExpenseComponent, exp)
      .subscribe((chExp) => {
        if (chExp) {
          const vem = new VehicleExpenceModel();
          vem.currency_lbl = chExp.currency_lbl;
          vem.exp_date = chExp.exp_date;
          vem.vehicle_id = chExp.vehicle_id;
          vem.exp_millage = chExp.exp_millage;
          vem.exp_note = chExp.exp_note;
          vem.track_id = chExp.track_id;
          vem.exp_price = chExp.exp_price;
          this.expenseService.addExpense(vem)
            .subscribe(
              (result: ResponseVehicleExpences) => this.processExpences(result),
              error => {
                this.toastr.error(error);
              });
        }

      });
  }

  getExpenseById(expenseId: number): VehicleExpenceModel {
    let retValue: VehicleExpenceModel;

    if (this.expences && this.expences.length > 0) {
      retValue = this.expences.find(x => x.exp_id === expenseId);
    }

    return retValue;
  }

  editExpence(expenseId: number): void {
    // console.log('editExpence-> expenseId ' + expenseId);
    const exp: VehicleExpenceModel = this.getExpenseById(expenseId);
    console.dir(exp);

    this.dialogService.addDialog(EditExpenseComponent, exp)
      .subscribe((chExp) => {
        if (chExp) {
          const vem = new VehicleExpenceModel();
          vem.currency_lbl = chExp.currency_lbl;
          vem.exp_date = chExp.exp_date;
          vem.vehicle_id = chExp.vehicle_id;
          vem.exp_millage = chExp.exp_millage;
          vem.exp_note = chExp.exp_note;
          vem.track_id = chExp.track_id;
          vem.exp_price = chExp.exp_price;
          vem.exp_id = chExp.exp_id;
          this.expenseService.updateExpense(vem)
            .subscribe(
              (result: ResponseVehicleExpences) => this.processExpences(result),
              error => {
                this.toastr.error(error);
              });
        }

      });
  }

  //#endregion

  //#endregion
}
