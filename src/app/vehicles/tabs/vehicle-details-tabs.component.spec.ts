import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VehicleDetailsTabsComponent } from './vehicle-details-tabs.component';

describe('VehicleDetailsTabsComponent', () => {
  let component: VehicleDetailsTabsComponent;
  let fixture: ComponentFixture<VehicleDetailsTabsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VehicleDetailsTabsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VehicleDetailsTabsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
