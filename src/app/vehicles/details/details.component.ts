import { Component, OnInit, animate, ViewContainerRef } from '@angular/core';
import { FormBuilder, FormGroup, Validators, AbstractControl, ValidatorFn } from '@angular/forms';
import { SpinnerService } from '../../@core/spinner/spinner.service';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { ToastsManager } from 'ng2-toastr';
import { DialogService } from 'ng2-bootstrap-modal';

// Servoces
import { VehicleService } from '../../@core/api/vehicle/vehicle.service';
import { CurrentUserService } from '../../@core/auth/current-user.service';
// Models
import { ResponseVehicle, VehicleModel, IVehicleModel, VehicleStat } from '../../@core/api/vehicle/dto';
import { EditVehicleComponent, IEditVehicleModel } from '../modals/edit-vehicle.component';
// Animations
import { Animate } from '../../@core/const/animation.const';
import { PageTransition, ShowHideCard } from '../../animation';
import { NgbDate } from '@ng-bootstrap/ng-bootstrap/datepicker/ngb-date';
import { ConfirmModal } from '../../@shared/modal/confirm/confirm.modal';
import { AppRoutes } from '../../@core/const/routes/app-routes.const';

const Wide = 'product-image-wide';
const Tall = 'product-image-tall';

@Component({
  selector: 'app-vehicle-details',
  templateUrl: './details.component.html',
  styleUrls: ['./details.component.css'],
  animations: [PageTransition, ShowHideCard]
})
export class VehicleDetailsComponent implements OnInit {

  imageNotAvailable = 'http://vlb.iordanov.info/images/Vehicle_Log_Book_Logo.png';


  constructor(private spinnerService: SpinnerService,
    private toastr: ToastsManager,
    private route: ActivatedRoute,
    private router: Router,
    private vehicleService: VehicleService,
    private currentUserService: CurrentUserService,
    private dialogService: DialogService) {
    // this.toastr.setRootViewContainerRef(vcr);
  }

  state: string = Animate.in;
  vehicleId: number;
  vehicle: VehicleModel;
  imageClass: string;
  private NgbModalOptions = { closeByClickingOutside: true, size: 'lg' };

  ngOnInit() {
    this.state = (this.state === Animate.in ? Animate.out : Animate.in);


    this.route.params
      .subscribe((params: Params) => {
        this.vehicleId = params['id'];
        this.getVehicle(this.vehicleId);
      });
  }

  public get imageUri(): string {
    let image = this.imageNotAvailable;
    if (this.vehicle && this.vehicle.image) {
      image = 'http://vlb.iordanov.info/img_upload/' + this.vehicle.image;
    }
    return image;
  }


  private getImageClass(): string {
    // tslint:disable-next-line:prefer-const
    let image = new Image();
    if (this.vehicle && this.vehicle.image) {
      image.src = this.imageUri;
    } else {
      image.src = this.imageNotAvailable;
    }

    if (image.width > image.height + 20 || image.width === image.height) {
      // return wide image class
      return Wide;
    } else {
      return Tall;
    }
  }

  onImageLoad() {
    this.imageClass = this.getImageClass();
  }

  //#region Modals

  public editVehicleClick() {

    this.dialogService.addDialog(EditVehicleComponent, this.vehicle)
      .subscribe((chVehicle) => {
        if (chVehicle) {
          if (this.vehicle) {
            console.dir(chVehicle);
            const vl = this.vehicle;
            vl.name = chVehicle.name;
            vl.model = chVehicle.model;
            vl.purchase_date = chVehicle.purchase_date;
            vl.first_reg = chVehicle.first_reg;
            vl.sale_date = chVehicle.sale_date;
            vl.milage_km_start = chVehicle.milage_km_start as number;
            vl.milage_km_current = chVehicle.milage_km_current as number;
            vl.track_in_mi = chVehicle.track_in_mi;

            this.vehicleService.editVehicle(vl)
              .subscribe(
                (result: ResponseVehicle) => {
                  if (result.status === 'success') {
                    const element: IVehicleModel = result.data.vehicle;
                    const tmp: VehicleModel = VehicleModel.fromJSON(element);
                    this.vehicle = tmp;
                    console.dir(tmp);
                    this.toastr.success('Vehicle updated!');
                  } else {
                    this.toastr.error(result.status, 'Error');
                  }
                },
                error => {
                  this.toastr.error(error);
                });
          }

        } else {
          return;

        }
      });
  }

  public deleteVehicleClick() {
    const dialog = {
      title: 'Confirm',
      message: 'Are you sure you want to delete vehicle number <b>' + this.vehicle.vehicleId + '</b>: ' + this.vehicle.name + '.',
      type: 'modal-warning'
    };
    this.dialogService.addDialog(ConfirmModal, dialog, this.NgbModalOptions)
      .subscribe((res) => {
        if (res) {
          this.vehicleService.deleteVehicle(this.vehicle.vehicleId)
            .subscribe((result: ResponseVehicle) => {
                this.toastr.success('Vehicle deleted!');
                this.router.navigate([AppRoutes.Vehicles]);
            },
              error => {
                this.toastr.error(error);
              });
        }
      });
  }


  //#endregion


  //#region Service Call Methods

  getVehicle(vehicleId: number): void {
    // this.spinnerService.display(true);
    this.vehicleService.getVehicle(vehicleId)
      .subscribe(
        (result: ResponseVehicle) => {

          // this.spinnerService.display(false);

          if (result.status === 'success') {
            if (result.data && result.data.vehicle) {
              const element: IVehicleModel = result.data.vehicle;

              const vehicle: VehicleModel = VehicleModel.fromJSON(element);
              let timeUsed = 0;
              if (vehicle.purchase_date && vehicle.purchase_date.date) {
                timeUsed = new Date().getTime() - vehicle.purchase_date.date.getTime();
              }
              if (vehicle.sale_date && vehicle.sale_date.date) {
                timeUsed = vehicle.sale_date.date.getTime() - vehicle.purchase_date.date.getTime();
              }
              vehicle.timeInUseMs = timeUsed;

              // Set Vehicle Statistic property
              if (result.data.vehicle_stat) {
                vehicle.vehicles_stat = VehicleStat.fromJSON(result.data.vehicle_stat);
              }
              this.vehicle = vehicle;
            }
            this.currentUserService.selectedVehicle = this.vehicle;

          } else {
            this.toastr.error(result.status, 'Error');
          }
        },
        error => {
          this.toastr.error(error);
        });
  }

  //#endregion

}
