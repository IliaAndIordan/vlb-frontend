import { Component, OnInit, OnDestroy, EventEmitter, HostListener, Input, Output, ViewContainerRef } from '@angular/core';
import { FormBuilder, FormGroup, Validators, AbstractControl, ValidatorFn } from '@angular/forms';
import { trigger, state, style, animate, transition } from '@angular/animations';
import { Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

// Services
import { SpinnerService } from '../@core/spinner/spinner.service';
import { VehicleService } from '../@core/api/vehicle/vehicle.service';
import { ToastsManager } from 'ng2-toastr';
// Models
import { VehicleModel, ResponseVehicles, VehicleStat, DateModel} from '../@core/api/vehicle/dto';
import { IDateModel, IVehicleStat, IVehicleModel, ResponseVehicle } from '../@core/api/vehicle/dto';

import { CurrentUserService } from '../@core/auth/current-user.service';
import { AppRoutes } from '../@core/const/routes/app-routes.const';
// Animations
import { Animate } from '../@core/const/animation.const';
import { PageTransition, ShowHideTriggerBlock, ViewMoreCardTrigger, ShowHideTriggerFlex } from '../animation';
import { ShowPanelTrigger, ShowHideCardTrigger, ShowHideListTrigger } from '../animation';
import { DialogService } from 'ng2-bootstrap-modal';
import { IEditVehicleModel, EditVehicleComponent } from './modals/edit-vehicle.component';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  animations: [PageTransition,
    ShowHideTriggerFlex,
    ShowPanelTrigger,
    ShowHideListTrigger,
    ShowHideCardTrigger,
    ShowHideTriggerBlock]
})
export class HomeComponent implements OnInit {
  constructor(private router: Router,
    private spinerService: SpinnerService,
    // private vcr: ViewContainerRef,
    private vehicleService: VehicleService,
    private toastr: ToastsManager,
    private currentUserService: CurrentUserService,
    private modalService: NgbModal,
    private dialogService: DialogService) {
    // this.toastr.setRootViewContainerRef(vcr);
  }

  // Animations
  state: string = Animate.in;
  vehicles: VehicleModel[];
  maxDistance: number = 0;
  maxTimeInUseMls: number = 0;
  maxTotalFuel: number = 0;
  maxAvgFuelPrice: number = 0;
  maxTotalExpences: number = 0;

  ngOnInit() {
    // Animation
    this.state = (this.state === Animate.in ? Animate.out : Animate.in);
    this.getUserVehicles();
  }

  onDetailsViewClick(id): void {
    console.log('onDetailsViewClick: ' + id);
    this.router.navigate([AppRoutes.Vehicles + '/', id]);
  }

  getUserVehicles(): void {
    this.vehicleService.getVehicles()
      .subscribe(
        (result: ResponseVehicles) => {
          if (result.status === 'success') {
            this.vehicles = [];
            this.maxDistance = 0;
            this.maxTimeInUseMls = 0;
            this.maxTotalFuel = 0;
            this.maxAvgFuelPrice = 0;
            this.maxTotalExpences = 0;

            for (let idx = 0; idx < result.data.vehicles.length; idx++) {
              const element: IVehicleModel = result.data.vehicles[idx];

              const tmp: VehicleModel = VehicleModel.fromJSON(element);
              //  // Object.assign(new VehicleModel(), element);
              if (this.maxDistance < (tmp.milage_km_current - tmp.milage_km_start)) {
                this.maxDistance = (tmp.milage_km_current - tmp.milage_km_start);
              }

              let timeUsed = 0;
              if (tmp.purchase_date && tmp.purchase_date.date) {
                timeUsed = new Date().getTime() - tmp.purchase_date.date.getTime();
              }
              if (tmp.sale_date && tmp.sale_date.date) {
                timeUsed = tmp.sale_date.date.getTime() - tmp.purchase_date.date.getTime();
              }
              tmp.timeInUseMs = timeUsed;
              if (this.maxTimeInUseMls < timeUsed) {
                this.maxTimeInUseMls = timeUsed;
              }

              // Set Vehicle Statistic property
              if (result.data.vehicles_stat) {
                const stat = result.data.vehicles_stat.find(x => x.vehicle_id === element.vehicleId);
                // console.dir(stat);
                if (stat) {
                  tmp.vehicles_stat = VehicleStat.fromJSON(stat); // Object.assign(new VehicleStat(), stat);
                }
              }

              if (tmp.vehicles_stat && tmp.vehicles_stat.avg_gas_price &&
                this.maxAvgFuelPrice < tmp.vehicles_stat.avg_gas_price) {
                this.maxAvgFuelPrice = tmp.vehicles_stat.avg_gas_price;
              }

              if (tmp.vehicles_stat && tmp.vehicles_stat.total_gas_quality &&
                this.maxTotalFuel < tmp.vehicles_stat.total_gas_quality) {
                this.maxTotalFuel = tmp.vehicles_stat.total_gas_quality;
              }

              if (tmp.vehicles_stat && tmp.vehicles_stat.total_expences &&
                this.maxTotalExpences < tmp.vehicles_stat.total_expences) {
                this.maxTotalExpences = tmp.vehicles_stat.total_expences;
              }

              this.vehicles.push(tmp);
            }
            this.currentUserService.vehicles = this.vehicles;
          } else {
            this.toastr.error(result.status, 'Error');
          }
        },
        error => {
          this.toastr.error(error);
        });
  }

  //#region Modals

  public newVehicleClick() {

    this.dialogService.addDialog(EditVehicleComponent, {})
      .subscribe((chVehicle: IEditVehicleModel) => {
        if (chVehicle) {
          const vl = new VehicleModel();
          console.dir(chVehicle);

          vl.name = chVehicle.name;
          vl.model = chVehicle.model;
          vl.purchase_date = chVehicle.purchase_date;
          vl.first_reg = chVehicle.first_reg;
          vl.sale_date = chVehicle.sale_date;
          vl.milage_km_start = chVehicle.milage_km_start as number;
          vl.milage_km_current = chVehicle.milage_km_current as number;
          vl.track_in_mi = chVehicle.track_in_mi;

          this.vehicleService.newVehicle(vl)
            .subscribe(
              (result: ResponseVehicle) => {
                if (result.status === 'success') {
                  const element: IVehicleModel = result.data.vehicle;
                  const tmp: VehicleModel = VehicleModel.fromJSON(element);
                  this.getUserVehicles();
                  this.toastr.success('Vehicle created!');
                } else {
                  this.toastr.error(result.status, 'Error');
                }
              },
              error => {
                this.toastr.error(error);
              });
        }
      });
  }


  //#endregion

}
