import { NgModule } from '@angular/core';
import { RouterModule, Routes, PreloadAllModules, NoPreloading } from '@angular/router';

import { PreloadSelectedModuleList } from './@core/preload-strategy';
import { AutGuard } from './@core/guards/aut-guard.service';
import { AppRoutes } from './@core/const/routes/app-routes.const';

// Components
import { HomeComponent } from './home/home.component';
import { UserProfileComponent } from './user-profile/user-profile.component';
import { VehiclesComponent } from './vehicles/vehicles.component';

export const routes: Routes = [
  { path: '', pathMatch: 'full', redirectTo: AppRoutes.Home, data: { title: 'Home' } },
  { path: AppRoutes.Home, component: HomeComponent, data: { title: 'Home' } },
  {
    path: AppRoutes.UserProfile, component: UserProfileComponent, canActivate: [AutGuard],
    data: { title: 'Profile', group: 'Profile', divider: false }
  },
  /*{ path: AppRoutes.Vehicles, component: VehiclesComponent, canActivate: [AutGuard],
    data: { title: 'Vehicles', icon: 'icon-info', group: 'Vehicles', divider: false }
  }*/
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { preloadingStrategy: PreloadSelectedModuleList })],
  exports: [RouterModule],
  providers: [PreloadSelectedModuleList]
})
export class AppRoutingModule { }

export const routableComponents = [
  UserProfileComponent
];
