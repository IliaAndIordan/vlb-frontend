import { Component, OnInit } from '@angular/core';
// Services
import { CurrentUserService } from '../@core/auth/current-user.service';
// Models
import { UserModel } from '../@core/auth/api/dtos';
import { AuthService } from '../@core/auth/api/auth.service';
import { Router } from '@angular/router';
import { AppRoutes } from '../@core/const/routes/app-routes.const';

@Component({
  selector: 'app-user-profile',
  templateUrl: './user-profile.component.html',
  styleUrls: ['./user-profile.component.css']
})
export class UserProfileComponent implements OnInit {

  public user: UserModel;
  public avatarUrl: string;
  constructor(private router: Router,
    private currentUserService: CurrentUserService,
    private authServuce: AuthService) { }

  ngOnInit() {
    this.user = this.currentUserService.user;
    this.avatarUrl = this.currentUserService.avatarUrl;
  }

  logout() {
    this.authServuce.logout();
    this.router.navigate([AppRoutes.Login]);
  }

}
