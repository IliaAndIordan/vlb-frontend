import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-sidebar-nav-dropdown',
  templateUrl: './sidebar-nav-dropdown.component.html',
  styleUrls: ['./sidebar-nav-dropdown.component.css']
})
export class SidebarNavDropdownComponent implements OnInit {

  @Input() link: any;

  public isBadge() {
    return this.link.badge ? true : false;
  }

  public isIcon() {
    return this.link.icon ? true : false;
  }

  constructor() { }

  ngOnInit() {
  }

}
