import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';
// Components

@Component({
  selector: 'app-sidebar-nav-item',
  templateUrl: './sidebar-nav-item.component.html',
  styleUrls: ['./sidebar-nav-item.component.css']
})

export class SidebarNavItemComponent implements OnInit {

  @Input() item: any;

  constructor(private router: Router) {
  }

  ngOnInit() {
  }

  public hasClass() {
    return this.item.class ? true : false;
  }

  public isDropdown() {
    return this.item.children ? true : false;
  }

  public thisUrl() {
    return this.item.url;
  }

  public isActive() {
    return this.router.isActive(this.thisUrl(), false);
  }



}

