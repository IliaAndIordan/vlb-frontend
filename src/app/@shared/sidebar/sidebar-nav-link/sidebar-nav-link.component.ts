import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-sidebar-nav-link',
  templateUrl: './sidebar-nav-link.component.html',
  styleUrls: ['./sidebar-nav-link.component.css']
})
export class SidebarNavLinkComponent implements OnInit {

  @Input() link: any;

  constructor() {
    // this language will be used as a fallback when a translation isn't found in the current language
    // translate.setDefaultLang('en');

    // the lang to use, if the lang isn't available, it will use the current loader to get them
   // translate.use('en');
/*
   translate.get('Home').subscribe((res: string) => {
    console.log('ML: ' + this.translate.get(res));
});*/
  }

  ngOnInit() {
  }

  public hasVariant() {
    return this.link.variant ? true : false;
  }

  public isBadge() {
    return this.link.badge ? true : false;
  }

  public isExternalLink() {
    return this.link.url.substring(0, 4) === 'http' ? true : false;
  }

  public isIcon() {
    return this.link.icon ? true : false;
  }

  public hideMobile() {
    if (document.body.classList.contains('sidebar-mobile-show')) {
      document.body.classList.toggle('sidebar-mobile-show');
    }
  }


}
