import { Component } from '@angular/core';
import { Router, ActivatedRoute, NavigationEnd, Params, PRIMARY_OUTLET } from '@angular/router';
import 'rxjs/add/operator/filter';
// Services
import { CurrentUserService } from '../../../@core/auth/current-user.service';
// Models
import { AppRoutes } from '../../../@core/const/routes/app-routes.const';
import { Route } from '@angular/compiler/src/core';


export const NAVIGATION = [
  {
    name: 'Home',
    url: '/home',
    icon: 'icon-home',
    authorised: false
  },
  {
    name: 'Dashboard',
    url: '/' + AppRoutes.Dashboard,
    icon: 'icon-speedometer',
    authorised: true
  },
  {
    divider: true
  },
  {
    title: true,
    name: 'User',
    authorised: false
  },
  {
    name: 'Login',
    url: 'login',
    icon: 'icon-login',
    authorised: false
  },
  {
    title: true,
    name: 'Vehicles',
    authorised: true
  },
  {
    name: 'Vehicles',
    url: '/' + AppRoutes.Vehicles,
    icon: 'icon-speedometer',
    authorised: true
  },
  {
    title: true,
    name: 'Information',
    authorised: 'undefined'
  },
  {
    name: 'Profile',
    url: '/' + AppRoutes.UserProfile,
    icon: 'icon-info',
    authorised: true
  },
  {
    name: 'About',
    url: '/home/about',
    icon: 'icon-info',
    authorised: false
  },
  {
    name: 'Roadmap ',
    url: '/home/roadmap',
    icon: 'icon-direction',
    authorised: false
  },
  {
    name: 'Functionality',
    url: '/home/functionality',
    icon: 'icon-puzzle',
    authorised: false
  }
];


@Component({
  selector: 'app-sidebar-nav',
  templateUrl: './sidebar-nav.component.html',
  styleUrls: ['./sidebar-nav.component.css']
})

export class SidebarNavComponent {


  public navigation = NAVIGATION;

  constructor(private router: Router,
    private currentUserService: CurrentUserService,
    private route: ActivatedRoute) {
  }

  public isDivider(item) {
    return item.divider ? true : false;
  }

  public isTitle(item) {
    return item.title ? true : false;
  }

  public isVisible(item) {
    let visible = true;
    if (item.authorised !== 'undefined') {
      visible = (item.authorised === this.currentUserService.isAuthenticated);
    }
    // visible = (item.authorised === this.currentUserService.isAuthenticated);

    return visible;
  }
}
