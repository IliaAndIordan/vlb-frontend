import { Component, OnInit, OnDestroy, EventEmitter, HostListener, Input, Output, ViewContainerRef } from '@angular/core';
import { trigger, state, style, animate, transition } from '@angular/animations';
// Constants
import { Language } from '../../@core/const/language.const';
import { Animate } from '../../@core/const/animation.const';
// Models
import { UserViewModel } from '../view-models/user.view-model';
import { CurrentUserService } from '../../@core/auth/current-user.service';
import { TranslateService } from '@ngx-translate/core';
import { LanguageName } from '../../@core/const/language.names.const';
import { UserModel } from '../../@core/auth/api/dtos';


@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  public user: UserModel;
  private userChangedSubscription: any;

  public languages: string[];

  constructor(private cus: CurrentUserService,
    private ml: TranslateService) { }

  ngOnInit() {
    this.languages = this.ml.getLangs();
    this.user = this.cus.user;
    this.userChangedSubscription = this.cus.UserChanged
      .subscribe(newUser => {
        this.user = newUser;
      });
  }

  //#region Multilanguage

  public get curLanguage(): string {
    let rv = 'bg';

    if (this.ml.currentLang) {
      rv = this.ml.currentLang;
    }
    // console.log('curLanguage');
    return rv;
  }

  public get curLanguageIconClass(): string {
    let rv = 'flag-icon flag-icon-bg';
    if (this.ml.currentLang) {
      rv = 'flag-icon flag-icon-' + (this.ml.currentLang === Language.en ? 'gb' : this.ml.currentLang);
    }
    // console.log('curLanguageIconClass');
    return rv;
  }

  public clickLnguageOption(lng) {
    // console.log('clickLnguageOption');
    this.ml.use(lng);
  }

  public getLanguageName(lng) {
    // console.log('getLanguageName');
    let rv = 'en';
    if (lng) {
      rv = LanguageName[lng];
      /*
      this.ml.get(rv).subscribe(
        label => {
          rv = label;
        }
      );*/
    }
    return rv;
  }

  public getLanguageIconClass(lng): string {
    let rv = this.curLanguageIconClass;
    if (lng) {
      rv = 'flag-icon flag-icon-' + (lng === Language.en ? 'gb' : lng);
    }
    // console.log('getLanguageIconClass');
    return rv;
  }

  //#endregion

  //#region User

  public get avatarUrl(): string {
    // console.log('avatarUrl');
    return this.cus.avatarUrl;
  }
  //#endregion

}

