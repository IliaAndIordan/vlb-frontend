import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ChartjsComponent } from './chartjs.component';

const routes: Routes = [
    {
        path: 'charts',
        component: ChartjsComponent,
        data: {
            title: 'Charts'
        }
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class ChartjsRoutingModule { }
