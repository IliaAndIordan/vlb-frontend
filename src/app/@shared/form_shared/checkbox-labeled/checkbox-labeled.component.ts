import { Component, OnInit, EventEmitter, Input, Output, ViewChild } from '@angular/core';

@Component({
  selector: 'app-checkbox-labeled',
  templateUrl: './checkbox-labeled.component.html',
  styleUrls: ['./checkbox-labeled.component.css']
})
export class CheckboxLabeledComponent implements OnInit {

  constructor() { }

  /**
   * BINDINGS
   */
  @Input() label: string;
  @Input() cssId: string;
  @Input() checked: boolean;
  @Input() disabled: boolean = false;
  // tslint:disable-next-line:no-output-on-prefix
  @Output() onChecked: EventEmitter<boolean> = new EventEmitter<boolean>();

  /**
   * FIELDS
   */
  @ViewChild('input') input;

  ngOnInit() { }

  handleCheck(checked: boolean) {
    // What we are sending to the event handler that is being passed to parent function.
    this.checked = checked;
    this.onChecked.emit(checked);
  }

  public uncheckBox() {
    this.checked = false;
    this.input.nativeElement.checked = false;
  }

  public checkBox() {
    this.checked = true;
    this.input.nativeElement.checked = true;
  }

  public ischecked(): boolean {
    return this.checked;
  }

}
