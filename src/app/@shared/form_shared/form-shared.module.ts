import { NgModule, Optional, SkipSelf } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

// Feechure Modules
import { FormInputsModule } from './inputs/form-inputs.module';
import { ButtonsModule } from './buttons/buttons.module';
import { CheckboxComponent } from './checkbox/checkbox.component';
import { CheckboxLabeledComponent } from './checkbox-labeled/checkbox-labeled.component';
import { SwitchLabelComponent } from './switch-label/switch-label.component';

// Components

@NgModule({

  imports: [
    CommonModule,
    RouterModule,
    ButtonsModule,
    FormInputsModule,
  ],
  declarations: [
    CheckboxComponent,
    CheckboxLabeledComponent,
    SwitchLabelComponent
  ],
  exports: [
    ButtonsModule,
    FormInputsModule,
    // Components
    CheckboxComponent,
    CheckboxLabeledComponent,
    SwitchLabelComponent
  ]
})
export class FormSharedModule {

  constructor() { }
}
