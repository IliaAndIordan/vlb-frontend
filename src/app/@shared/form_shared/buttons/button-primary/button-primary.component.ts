import { Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
  selector: 'app-button-primary',
  templateUrl: './button-primary.component.html',
})
export class ButtonPrimaryComponent {

  constructor() { }

  // Bindings
  @Input() className: string;
  @Input() type: string;
  @Input() color: string;
  @Input() disabled: boolean;

  // the name of the event handler of the component selector
  // tslint:disable-next-line:no-output-on-prefix
  @Output() onClick: EventEmitter<any> = new EventEmitter<any>();

  handleClick(event: any) {
    // what we sending to the event handler that is being passed to parent function
    this.onClick.emit('emit');
  }
}
