import { Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
  selector: 'app-button-info',
  templateUrl: './button-info.component.html',
  styleUrls: ['./button-info.component.css']
})
export class ButtonInfoComponent {

  /////////////////
  // CONSTRUCTOR //
  /////////////////
  constructor() { }

  //////////////
  // BINDINGS //
  //////////////
  // tslint:disable-next-line:no-output-on-prefix
  @Output() onHover: EventEmitter<any> = new EventEmitter<any>();
  @Output() offHover: EventEmitter<any> = new EventEmitter<any>();
  @Input() message: string;
  @Input() placement: string;

  ////////////
  // FIELDS //
  ////////////



}
