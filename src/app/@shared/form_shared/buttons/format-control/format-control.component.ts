import { Component, EventEmitter, Input, Output, OnInit } from '@angular/core';

@Component({
  selector: 'app-format-control',
  templateUrl: './format-control.component.html',
  styleUrls: ['./format-control.component.css']
})
export class FormatControlComponent implements OnInit {

  /////////////////
  // CONSTRUCTOR //
  /////////////////
  constructor() { }

  //////////////
  // BINDINGS //
  //////////////
  @Input() icon: string;
  @Input() label: string;
  @Input() color: string;
  @Input() large: boolean;

  ////////////
  // FIELDS //
  ////////////
  labeled: boolean;

  ngOnInit() {

    if (this.label) {
      this.labeled = true;
    } else {
      this.labeled = false;
    }

  }

}
