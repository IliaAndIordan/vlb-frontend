import { Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
  selector: 'app-button-pill',
  templateUrl: './button-pill.component.html',
  styleUrls: ['./button-pill.component.css']
})
export class ButtonPillComponent {
  /////////////////
  // CONSTRUCTOR //
  /////////////////
  constructor() { }

  //////////////
  // BINDINGS //
  //////////////
  @Input() className: string;
  @Input() type: string;
  @Input() identification: string;
  @Input() icon: string;
  @Input() title: string;
  @Input() light: boolean;

  ////////////
  // FIELDS //
  ////////////

}
