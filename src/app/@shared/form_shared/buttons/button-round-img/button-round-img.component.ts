import { Component, EventEmitter, Input, Output } from '@angular/core';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-button-round-img',
  templateUrl: './button-round-img.component.html'
})
export class ButtonRoundImgComponent {

  constructor() { }
  //////////////
  // BINDINGS //
  //////////////
  @Input() icon: string;
  @Input() large: boolean;
  @Input() message: string;
  @Input() placement: string;
  // the name of the even handler on the component selector
  // tslint:disable-next-line:no-output-on-prefix
  @Output() onClick: EventEmitter<any> = new EventEmitter<any>();

  ////////////
  // FIELDS //
  ////////////

  handleClick(event: any) {
    // what we are sending to the event handler that is being passed to parent function.
    this.onClick.emit('Event emitted!');
  }

}
