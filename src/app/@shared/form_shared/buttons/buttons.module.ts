import { NgModule, Optional, SkipSelf } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
// Components
import { ButtonPrimaryComponent } from './button-primary/button-primary.component';
import { ButtonRoundComponent } from './button-round/button-round.component';
import { ButtonFlatComponent } from './button-flat/button-flat.component';
import { FormatControlComponent } from './format-control/format-control.component';
import { ButtonPillComponent } from './button-pill/button-pill.component';
import { ButtonInfoComponent } from './button-info/button-info.component';
import { ButtonRoundImgComponent } from './button-round-img/button-round-img.component';
@NgModule({
  imports: [
    NgbModule.forRoot(),
    CommonModule
  ],
  declarations: [
    ButtonPrimaryComponent,
    ButtonRoundComponent,
    ButtonFlatComponent,
    FormatControlComponent,
    ButtonPillComponent,
    ButtonInfoComponent,
    ButtonRoundImgComponent
  ],
  exports: [
    ButtonPrimaryComponent,
    ButtonRoundComponent,
    ButtonFlatComponent,
    FormatControlComponent,
    ButtonPillComponent,
    ButtonInfoComponent,
    ButtonRoundImgComponent
  ]
})
export class ButtonsModule {
  constructor() { }
}
