import { Component, EventEmitter, Input, Output } from '@angular/core';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';


@Component({
  selector: 'app-button-flat',
  templateUrl: './button-flat.component.html',
  styleUrls: ['./button-flat.component.css']
})
export class ButtonFlatComponent {

  constructor() { }
  /**
    * BINDINGS
    */
  @Input() type: string;
  @Input() color: string;
  @Input() icon: string;
  @Input() disabled: boolean;
  @Input() noPadding: boolean;

  // tslint:disable-next-line:no-output-on-prefix
  @Output() onClick: EventEmitter<any> = new EventEmitter<any>();

  /**
   * FIELDS
   */

  handleClick(event: any) {
    // what we are sending to the event handler that is being passed to parent function.
    this.onClick.emit('emit!');
  }

}
