import { Component, EventEmitter, Input, Output } from '@angular/core';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-button-round',
  templateUrl: './button-round.component.html',
  styleUrls: ['./button-round.component.css']
})
export class ButtonRoundComponent {

  constructor() { }
  //////////////
  // BINDINGS //
  //////////////
  @Input() icon: string;
  @Input() large: boolean;
  @Input() message: string;
  @Input() placement: string;
  // the name of the even handler on the component selector
  // tslint:disable-next-line:no-output-on-prefix
  @Output() onClick: EventEmitter<any> = new EventEmitter<any>();

  ////////////
  // FIELDS //
  ////////////

  handleClick(event: any) {
    // what we are sending to the event handler that is being passed to parent function.
    this.onClick.emit('Event emitted!');
  }

}
