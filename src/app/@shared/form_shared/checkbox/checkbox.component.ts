import { Component, OnInit, EventEmitter, Input, Output } from '@angular/core';

@Component({
  selector: 'app-checkbox',
  templateUrl: './checkbox.component.html',
  styleUrls: ['./checkbox.component.css']
})
export class CheckboxComponent implements OnInit {

  /////////////////
  // CONSTRUCTOR //
  /////////////////
  constructor() { }

  //////////////
  // BINDINGS //
  //////////////
  @Input() cssId: string;
  // tslint:disable-next-line:no-output-on-prefix
  @Output() onChecked: EventEmitter<boolean> = new EventEmitter<boolean>();
  @Input() checked: boolean;


  ////////////
  // FIELDS //
  ////////////


  ngOnInit() { }

  handleCheck(checked: boolean): void {
    // what we are sending to the event handler that is being passed to parent function.
    this.onChecked.emit(checked);
  }


}
