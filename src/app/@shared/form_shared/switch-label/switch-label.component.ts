import { Component, OnInit, EventEmitter, Input, Output, ViewChild } from '@angular/core';
import {FormControl, FormGroup, Validators, FormControlName} from '@angular/forms';

@Component({
  selector: 'app-switch-label',
  templateUrl: './switch-label.component.html',
  styleUrls: ['./switch-label.component.css']
})
export class SwitchLabelComponent implements OnInit {

  constructor() { }

  /**
   * BINDINGS
   */
  @Input() label: string;
  @Input() labelOn: string = 'On';
  @Input() labelOff: string = 'Off';
  @Input() cssId: string;
  @Input() checked: boolean;
  @Input() disabled: boolean = false;
  @Input() className: string;

  // tslint:disable-next-line:no-output-on-prefix
  @Output() onChecked: EventEmitter<boolean> = new EventEmitter<boolean>();

  /**
   * FIELDS
   */
  @ViewChild('input') input;

  ngOnInit() { }

  handleCheck(checked: boolean) {
    // What we are sending to the event handler that is being passed to parent function.
    this.checked = checked;
    this.onChecked.emit(checked);
  }

  public uncheckBox() {
    this.checked = false;
    this.input.nativeElement.checked = false;
  }

  public checkBox() {
    this.checked = true;
    this.input.nativeElement.checked = true;
  }

  public ischecked(): boolean {
    return this.checked;
  }
}
