import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormControl, FormGroup, ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';

import { Validator } from '../../../../@core/validation/validator';

@Component({
  selector: 'app-form-field-label',
  templateUrl: './form-field-label.component.html',
})

// tslint:disable-next-line:component-class-suffix
export class FormFieldLabel implements OnInit {

  constructor() { }

  @Input() label: string;
  /*@Input() type: string;*/
  @Input() validator: Validator;
  @Input() error: string;
  @Input() prependIcon: string;
  @Input() prependText: string;
  @Input() controlName: string;
  @Input() helpText: string;
  @Input() grpClass: string;



  ngOnInit() {
  }
}
