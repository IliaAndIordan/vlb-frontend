import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormControl, FormGroup, ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
import { Validator } from '../../../../@core/validation/validator';

@Component({
  selector: 'app-form-field',
  templateUrl: './form-field.component.html',
})
// tslint:disable-next-line:component-class-suffix
export class FormField implements OnInit {

  constructor() { }

  @Input() label: string;
  @Input() type: string;
  @Input() validator: Validator;
  @Input() error: string;
  @Input() prependIcon: string;


  ngOnInit() {
  }

}
