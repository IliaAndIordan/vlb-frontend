import { NgModule, Optional, SkipSelf } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TranslateModule } from '@ngx-translate/core';

// Imputs
import { FormField } from './form-field/form-field.component';
import { FormFieldLabel } from './form-field-label/form-field-label.component';
import { TextareaField } from './textarea-field/textarea-field.component';

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    TranslateModule,
  ],
  declarations: [
    FormField,
    FormFieldLabel,
    TextareaField,
  ],
  exports: [
    FormField,
    FormFieldLabel,
    TextareaField,
  ]
})
export class FormInputsModule {
  constructor() { }
}
