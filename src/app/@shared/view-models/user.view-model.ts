export class UserViewModel {
    firstName: string;
    lastName: string;
    email: string;
    avatarUrl: string;
}
