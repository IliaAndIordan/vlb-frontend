import { NgModule, Optional, SkipSelf } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BrowserModule } from '@angular/platform-browser';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { BootstrapModalModule } from 'ng2-bootstrap-modal';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';  // <-- #1 import module

import { RouterModule } from '@angular/router';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { TabsModule } from 'ngx-bootstrap/tabs';
import { AvatarModule } from 'ngx-avatar';

// Carousel Component
import { CarouselModule } from 'ngx-bootstrap/carousel';

// Components
import { NavComponent } from './nav/nav.component';
import { HeaderComponent } from './header/header.component';
import { SidebarComponent } from './sidebar/sidebar.component';
import { SidebarHeaderComponent } from './sidebar/sidebar-header/sidebar-header.component';
import { SidebarNavComponent } from './sidebar/sidebar-nav/sidebar-nav.component';
import { SidebarNavItemComponent } from './sidebar/sidebar-nav-item/sidebar-nav-item.component';
import { SidebarNavLinkComponent } from './sidebar/sidebar-nav-link/sidebar-nav-link.component';
import { SidebarNavDropdownComponent } from './sidebar/sidebar-nav-dropdown/sidebar-nav-dropdown.component';
import { SidebarNavTitleComponent } from './sidebar/sidebar-nav-title/sidebar-nav-title.component';
import { ChartjsComponent } from './chartjs/chartjs.component';
import { ChartjsRoutingModule } from './chartjs/chartjs-routing.module';
// Feechure Modules
import { ChartsModule } from 'ng2-charts';
import { MainModule } from './main/main.module';
import { SpinnerComponent } from './spinner/spinner.component';

// Directive
import { SIDEBAR_TOGGLE_DIRECTIVES } from './sidebar/sidebar.directive';
import { TranslateModule } from '@ngx-translate/core';
// Components
import { ConfirmModal } from './modal/confirm/confirm.modal';


@NgModule({
  declarations: [
    NavComponent,
    HeaderComponent,
    SidebarComponent,
    SidebarHeaderComponent,
    SidebarNavComponent,
    SidebarNavItemComponent,
    SidebarNavLinkComponent,
    SidebarNavDropdownComponent,
    SidebarNavTitleComponent,
    ChartjsComponent,
    SpinnerComponent,
    SIDEBAR_TOGGLE_DIRECTIVES,
    ConfirmModal,
  ],
  imports: [
    CommonModule,
    BrowserModule,
    NgbModule,
    BootstrapModalModule.forRoot({ container: document.body }),
    ReactiveFormsModule,
    TranslateModule,
    FormsModule,
    AvatarModule,
    RouterModule,
    ChartsModule,
    MainModule,
    CarouselModule,
    ChartjsRoutingModule,
    BsDropdownModule.forRoot(),
    TabsModule.forRoot(),
    CarouselModule.forRoot(),
  ],
  exports: [
    CommonModule,
    BrowserModule,
    BootstrapModalModule,
    NgbModule,
    ReactiveFormsModule,
    FormsModule,
    TranslateModule,
    AvatarModule,
    ChartsModule,
    MainModule,
    CarouselModule,
    NavComponent,
    HeaderComponent,
    SidebarComponent,
    SidebarHeaderComponent,
    SidebarNavComponent,
    SidebarNavItemComponent,
    SidebarNavLinkComponent,
    SidebarNavDropdownComponent,
    SidebarNavTitleComponent,
    ChartjsComponent,
    SpinnerComponent,
    ConfirmModal,
  ],
  entryComponents: [
    ConfirmModal,
  ]
})
export class SharedModule {

  constructor() { }
}
