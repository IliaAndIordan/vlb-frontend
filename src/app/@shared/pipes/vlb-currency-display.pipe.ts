import { Pipe, PipeTransform } from '@angular/core';
import { CurrencyPipe } from '@angular/common';
import { CurrencyType } from '../../@core/const/currency.type';

/**
 * Get Curreny code based on Trade Service Currency Id
 */
@Pipe({ name: 'vlbcurrencydisplay' })
export class VlbCurrencyDisplayPipe implements PipeTransform {

    constructor(private currencyPipe: CurrencyPipe) { }

    transform(value: any, args?: any[], symbolDisplay?: boolean, digits?: string): string {
        let currencyCode = CurrencyType[value];
        if (!currencyCode) {
            console.error('no currencyCode provided to tscurrencydisplay pipe, defaulting to BG');
            currencyCode = CurrencyType.bg_BG; // USD
        }

        return currencyCode.toUpperCase();
    }
}
