import { Pipe, PipeTransform } from '@angular/core';
import { CurrencyPipe, getLocaleCurrencyName, getLocaleCurrencySymbol } from '@angular/common';
import { CurrencyType } from '../../@core/const/currency.type';

/**
 * Get Curreny code based on Trade Service Currency Id
 */
@Pipe({ name: 'vlbcurrency' })
export class VlbCurrencyPipe implements PipeTransform {

    constructor(private currencyPipe: CurrencyPipe) { }

    transform(value: any, args?: string, symbolDisplay?: boolean, digits?: string): string {
        let currencyTypeCode = CurrencyType.bg_BG; // Default BG
        let locale = 'en';
        if (symbolDisplay === null || symbolDisplay === undefined) {
            // make UI default to display currency symbol
            symbolDisplay = true;
        }

        if (!args || !args.length) { return this.currencyPipe.transform(value, currencyTypeCode, 'symbol', digits, locale); }

        if (args) {
            currencyTypeCode = this.getCurrencyCode(args);
            locale = args.replace('_', '-');
            const name = getLocaleCurrencyName(locale);
            const symbol = getLocaleCurrencySymbol(locale);
            return this.currencyPipe.transform(value, symbol, 'symbol-narrow', digits, locale);
        }

        return this.currencyPipe.transform(value, currencyTypeCode, 'symbol-narrow', digits);
    }

    public getCurrencyCode(currency_lbl: string): string {
        let rv = 'USD';
        switch (currency_lbl) {
            case 'bg_BG':
                rv = CurrencyType.bg_BG;
                break;
            case 'en_GB':
                rv = CurrencyType.en_GB;
                break;
            case 'de_DE':
                rv = CurrencyType.de_DE;
                break;
            default:
                rv = CurrencyType.en_US;
                break;
        }
        return rv;
    }
}

