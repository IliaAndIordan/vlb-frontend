import { DialogService } from 'ng2-bootstrap-modal/dist/dialog.service';
import { ConfirmModal } from './confirm.modal';
import { Observable } from 'rxjs/Observable';
import { Inject, forwardRef, ComponentFactoryResolver } from '@angular/core';

export class ConfirmModalService {

    // Modal
    private NgbModalOptions = { closeByClickingOutside: true, size: 'lg' };

    constructor(private resolver: ComponentFactoryResolver,
        @Inject(forwardRef(() => DialogService)) private dialogService: DialogService) { }

    public confirm(message: string, title: string = 'Confirm'): Observable<boolean> {
        // Config Message
        const dialog = {
            title: title,
            message: message
        };

        const obs = new Observable<boolean>(observer => {

            this.dialogService.addDialog(ConfirmModal, dialog, this.NgbModalOptions)
                .subscribe((res) => {
                    observer.next(res);
                    observer.complete();
                });
        });

        return obs;

    }
}
