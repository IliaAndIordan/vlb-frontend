import { Component, trigger, state, style, transition, animate, Input, OnInit } from '@angular/core';
import { DialogComponent, DialogService } from 'ng2-bootstrap-modal';
import { FormBuilder, FormGroup, FormControl, Validators, AbstractControl, ValidatorFn, FormArray } from '@angular/forms';
// Services
import { ConfirmAction } from '../../../@core/confirm-action';

@Component({
  templateUrl: './confirm.modal.html',
})

// tslint:disable-next-line:component-class-suffix
export class ConfirmModal extends DialogComponent<ConfirmAction, boolean> implements ConfirmAction, OnInit {

  constructor(dialogService: DialogService) {
    super(dialogService);
  }

  /**
  * Fields
  */
  public title: string;
  public message: string;
  public type: string;


  ngOnInit() { }

  public onConfirm() {
    this.result = true;
    this.close();
  }

  public onCancel() {
    this.result = false;
    this.close();
  }

}
