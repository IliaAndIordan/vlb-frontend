import { Component } from '@angular/core';
import { Router, ActivatedRoute, NavigationEnd, Params, PRIMARY_OUTLET, RouterEvent } from '@angular/router';
import 'rxjs/add/operator/filter';

@Component({
  selector: 'app-breadcrumbs',
  templateUrl: './app-breadcrumbs.component.html',
  styleUrls: ['./app-breadcrumbs.component.css']
})
export class AppBreadcrumbsComponent {

  constructor(private router: Router,
    private route: ActivatedRoute) {
    this.router.events.filter(event => event instanceof NavigationEnd).subscribe((event) => {
      this.breadcrumbs = [];

      const currentRoute = this.route;
      this.breadcrumbs = this.getBreadcrumbsParent(currentRoute);
      if (this.breadcrumbs) {
        this.breadcrumbs = this.breadcrumbs.reverse();
      }

      console.log('breadcrumbs: ');
      console.dir(this.breadcrumbs);
    });
  }
  // PRIMARY_OUTLET = 'primary';
  breadcrumbs: Array<Object>;

  private getBreadcrumbs(route: ActivatedRoute, url: string = '', breadcrumbs: IBreadcrumb[] = []): IBreadcrumb[] {
    const ROUTE_DATA_BREADCRUMB = 'title';

    // get the child routes
    const children: ActivatedRoute[] = route.children;
    // console.log('breadcrumbs-> children.length: ' + children.length);
    // return if there are no more children
    if (children.length === 0) {

      const breadcrumb: IBreadcrumb = {
        label: route.snapshot.data[ROUTE_DATA_BREADCRUMB],
        params: route.snapshot.params,
        url: url
      };
      breadcrumbs.push(breadcrumb);
      return breadcrumbs;
    }

    // iterate over each children
    for (const child of children) {
      // verify primary route
      if (child.outlet !== PRIMARY_OUTLET) {
        console.log('breadcrumbs-> PRIMARY_OUTLET: ' + child);
        continue;
      }
      // console.log('breadcrumbs-> hasOwnProperty: ' + child.snapshot.data.hasOwnProperty(ROUTE_DATA_BREADCRUMB));
      // verify the custom data property "title" is specified on the route
      if (!child.snapshot.data.hasOwnProperty(ROUTE_DATA_BREADCRUMB)) {

        return this.getBreadcrumbs(child, url, breadcrumbs);
      }

      // get the route's URL segment
      const routeURL: string = child.snapshot.url.map(segment => segment.path).join('/');
      // console.log('breadcrumbs-> routeURL: ' + routeURL);
      // append route URL to URL
      url += `/${routeURL}`;

      // add breadcrumb
      const breadcrumb: IBreadcrumb = {
        label: child.snapshot.data[ROUTE_DATA_BREADCRUMB],
        params: child.snapshot.params,
        url: url
      };
      // console.dir(breadcrumb);
      breadcrumbs.push(breadcrumb);

      // recursive
      return this.getBreadcrumbs(child, url, breadcrumbs);
    }

    return breadcrumbs;
  }

  private getBreadcrumbsParent(route: ActivatedRoute, url: string = '', breadcrumbs: IBreadcrumb[] = []): IBreadcrumb[] {
    const ROUTE_DATA_BREADCRUMB = 'title';

    if (route && route.outlet === PRIMARY_OUTLET) {

      if (route.snapshot.data.hasOwnProperty(ROUTE_DATA_BREADCRUMB)) {

        const routeURL: string = route.snapshot.url.map(segment => segment.path).join('/');
        // console.log('breadcrumbs-> routeURL: ' + routeURL);
        // append route URL to URL
        url += `/${routeURL}`;

        // add breadcrumb
        const breadcrumb: IBreadcrumb = {
          label: route.snapshot.data[ROUTE_DATA_BREADCRUMB],
          params: route.snapshot.params,
          url: url
        };

        // console.dir(breadcrumb);
        breadcrumbs.push(breadcrumb);
      }
    }
    // get the child routes
    const parent: ActivatedRoute = route.parent;
    // console.log('breadcrumbs-> children.length: ' + children.length);
    // return if there are no more children
    if (parent) {
      return this.getBreadcrumbsParent(parent, '', breadcrumbs);
    }

    return breadcrumbs;
  }
}

export interface IBreadcrumb {
  label: string;
  params?: Params;
  url: string;
}
