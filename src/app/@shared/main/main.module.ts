import { NgModule, Optional, SkipSelf } from '@angular/core';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { TabsModule } from 'ngx-bootstrap/tabs';
import { CommonModule } from '@angular/common';
import { ProgressbarModule } from 'ngx-bootstrap/progressbar';
import { TranslateModule, TranslateService, TranslatePipe, MissingTranslationHandler } from '@ngx-translate/core';
import { RouterModule } from '@angular/router';
import { Ng2OdometerModule } from 'ng2-odometer';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';

// Feechure Modules
import { FormSharedModule } from '../form_shared/form-shared.module';

// Pipes
import { TruncatePipe } from '../pipes/truncate.pipe';
import { VlbCurrencyPipe } from '../pipes/vlb-currency.pipe';
import { VlbCurrencyDisplayPipe } from '../pipes/vlb-currency-display.pipe';
// must import and an CurrencyPipe as a provide because it is injected into TsCurrencyPipe - Ogden 12-20-2017
import { CurrencyPipe } from '@angular/common';

// Components
import { AppBreadcrumbsComponent } from './app-breadcrumbs/app-breadcrumbs.component';
import { VehicleCardComponent } from './cards/vehicle-card/vehicle-card.component';
import { VehicleCardMillageComponent } from './cards/vehicle-card-millage/vehicle-card-millage.component';
import { VehicleCardDetailComponent } from './cards/vehicle-card-detail/vehicle-card-detail.component';
import { KeyValueBlockComponent } from './key-value-block/key-value-block.component';


@NgModule({
    imports: [
        CommonModule,
        NgbModule,
        TabsModule,
        RouterModule,
        TranslateModule,
        FormSharedModule,
        NgxDatatableModule,
        ProgressbarModule.forRoot(),
        Ng2OdometerModule.forRoot(),
    ],
    declarations: [
        // Pipes
        TruncatePipe,
        VlbCurrencyPipe,
        VlbCurrencyDisplayPipe,
        //
        AppBreadcrumbsComponent,
        VehicleCardComponent,
        VehicleCardMillageComponent,
        VehicleCardDetailComponent,
        KeyValueBlockComponent,
    ],
    exports: [
        // Pipes
        TruncatePipe,
        VlbCurrencyPipe,
        VlbCurrencyDisplayPipe,
        // Modules

        NgxDatatableModule,
        ProgressbarModule,
        TabsModule,
        AppBreadcrumbsComponent,
        VehicleCardComponent,
        FormSharedModule,
        // Components
        VehicleCardMillageComponent,
        VehicleCardDetailComponent,
        KeyValueBlockComponent,

    ],
    providers: [
        CurrencyPipe
    ]
})

export class MainModule {

    constructor() { }
}
