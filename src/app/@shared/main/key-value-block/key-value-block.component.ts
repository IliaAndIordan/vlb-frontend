import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

import { TruncatePipe } from '../../pipes/truncate.pipe';


@Component({
  selector: 'app-key-value-block',
  templateUrl: './key-value-block.component.html',
  styleUrls: ['./key-value-block.component.css']
})
export class KeyValueBlockComponent implements OnInit {


  constructor() { }
  /**
    * BINDINGS
    */
  @Input() key: string;
  @Input() infoMessage: string;
  @Input() value: string;
  // currency_lbl
  @Input() price: string;



  /**
   * FIELDS
   */

  ngOnInit() { }

  isNumber(val) { return typeof val === 'number'; }
  isDate(val) { return val instanceof Date; }

}
