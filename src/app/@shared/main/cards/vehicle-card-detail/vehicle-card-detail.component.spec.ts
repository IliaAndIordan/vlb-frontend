import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VehicleCardDetailComponent } from './vehicle-card-detail.component';

describe('VehicleCardDetailComponent', () => {
  let component: VehicleCardDetailComponent;
  let fixture: ComponentFixture<VehicleCardDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VehicleCardDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VehicleCardDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
