import { Component, OnDestroy, OnInit, Input } from '@angular/core';
// Services
import { CurrentUserService } from '../../../../@core/auth/current-user.service';
import { VehicleModel } from '../../../../@core/api/vehicle/dto';

@Component({
  selector: 'app-vehicle-card-detail',
  templateUrl: './vehicle-card-detail.component.html',
  styleUrls: ['./vehicle-card-detail.component.css']
})
export class VehicleCardDetailComponent implements OnInit {

  constructor(private currentUserService: CurrentUserService) { }

  @Input() vehicle: VehicleModel;

  ngOnInit() {
  }

  getTooltip(key: string): string {
    return VehicleDetailsTooltip[key];
  }
}

export const VehicleDetailsTooltip = {
  name: 'Vehicle Title or Registration Number',
  currency: 'The currenci change will not leed to the recallculations in the data. They are used only for conviniance visualization.',
  first_reg: 'Date of initial registration with the presence and support of the first vehicle s owner.',
  purchase_date: ' the date on which the purchased of vehicle becomes effective.',
  model: 'the name used by a manufacturer to market a range of similar cars.',
  milage_km_start: 'Mileage / Odometer on Start',
  track_in_mi: 'Track this vehicle s mileage in miles (default is kilometers)'
};

