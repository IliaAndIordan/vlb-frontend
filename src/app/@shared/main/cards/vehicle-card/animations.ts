import { trigger, state, style, transition, animate } from '@angular/animations';

export const HoverCardTrigger =
    trigger('hoverCardTrigger', [
        state('noHover', style({
            display: 'none',
            bottom: '22px',
            opacity: '0',
            height: '200px'
        })),
        state('hover', style({
            display: 'block',
            bottom: '6px',
            opacity: '1',
            height: '200px'
        })),
        transition('noHover <=> hover', animate('200ms'))
    ]);

