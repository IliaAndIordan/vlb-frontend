import { Component, OnDestroy, OnInit, Input, Output, EventEmitter, Inject, ViewChild } from '@angular/core';
import { getLocaleCurrencySymbol, getLocaleCurrencyName } from '@angular/common';
import { trigger, state, style, transition, animate } from '@angular/animations';
import { ActivatedRoute, Router } from '@angular/router';
import { DOCUMENT } from '@angular/common';
import { TranslateService, TranslatePipe, MissingTranslationHandler } from '@ngx-translate/core';

// Modules

// Models
import { VehicleModel, VehicleStat } from '../../../../@core/api/vehicle/dto';
import { Animate } from '../../../../@core/const/animation.const';
import { HoverCardTrigger } from './animations';
import { CurrentUserService } from '../../../../@core/auth/current-user.service';
import { AppRoutes } from '../../../../@core/const/routes/app-routes.const';

const Wide = 'product-card-image-wide';
const Tall = 'product-card-image-tall';

@Component({
  selector: 'app-vehicle-card',
  templateUrl: './vehicle-card.component.html',
  styleUrls: ['./vehicle-card.component.css'],
  animations: [HoverCardTrigger]
})
export class VehicleCardComponent implements OnInit {

  constructor(private currentUserService: CurrentUserService,
    private activeRoute: ActivatedRoute) { }

  /**
   * BINDINGS
   */
  @Input() vehicle: VehicleModel;
  @Input() showQty: boolean = false;
  @Input() index: string;
  @Input() maxDistance: number = 0;
  @Input() maxTimeInUseMls: number = 0;
  @Input() maxTotalFuel: number = 0;
  @Input() maxAvgFuelPrice: number = 0;
  @Input() maxTotalExpences: number = 0;

  // tslint:disable-next-line:no-output-on-prefix
  @Output() onClickQty: EventEmitter<any> = new EventEmitter<any>();
  @Input() checkbox: boolean = true;
  @Input() interactiveCard: boolean = true;
  @Output() detailsClick: EventEmitter<any> = new EventEmitter<any>();

  /**
  * FIELDS
  */
  imageClass: string;
  uniqueId: string;
  timeInUseMs: number = 0;

  // Modal Options.
  private NgbModalOptions = { closeByClickingOutside: true, size: 'lg' };
  // Animation Fields
  hoverState: string = Animate.noHover;
  // private positionedPriceSet: PositionedPriceSet;
  imageNotAvailable = 'http://vlb.iordanov.info/images/Vehicle_Log_Book_Logo.png';


  ngOnInit() {
    this.timeInUseMs = this.vehicle.timeInUseMs;
    // this.positionedPriceSet = this.cardHelper.getPositionedPriceSet(this.productItem.pricing);
  }

  hendleDetailsClick(vehicleId) {
    console.log('hendleDetailsClick: ' + vehicleId);
    this.detailsClick.emit(vehicleId);
  }

  getDetailsRouterLink(): string {
    const routeURL: string = this.activeRoute.snapshot.url.map(segment => segment.path).join('/');
    return  this.vehicle.vehicleId.toString();
  }

  //#region Properties

  public get hasXref(): boolean {
    return false;
  }

  public get imageUri(): string {
    let image = this.imageNotAvailable;
    if (this.vehicle && this.vehicle.image) {
      image = 'http://vlb.iordanov.info/img_upload/' + this.vehicle.image;
    }
    return image;
  }

  public get purchaseDate(): Date {
    let rv;
    if (this.vehicle && this.vehicle.purchase_date && this.vehicle.purchase_date.date) {
      rv = this.vehicle.purchase_date.date;
    }
    return rv;
  }


  public get firstRegistrationDate(): Date {
    let rv;
    if (this.vehicle && this.vehicle.first_reg && this.vehicle.first_reg.date) {
      rv = this.vehicle.first_reg.date;
    }
    return rv;
  }

  public get distance(): number {
    let rv = 0;
    if (this.vehicle) {
      rv = this.vehicle.milage_km_current - this.vehicle.milage_km_start;
    }
    return rv;
  }

  public get distanceMeasure(): string {
    let rv = 'km';
    if (this.vehicle && this.vehicle.track_in_mi) {
      rv = 'mi';
    }
    return rv;
  }
  public get distanceGasMeasure(): string {
    let rv = 'L';
    if (this.vehicle && this.vehicle.track_in_mi) {
      rv = 'Gal';
    }
    return rv;
  }

  public get distancePct(): number {
    let rv = 0;
    if (this.maxDistance > 0) {
      rv = Math.ceil((this.distance / this.maxDistance) * 100);
    }
    return rv;
  }

  public getTimeInUseMs(): number {
    let rv = 0;

    if (this.vehicle && this.vehicle.purchase_date && this.vehicle.purchase_date.date) {
      rv = new Date().getTime() - this.vehicle.purchase_date.date.getTime();
    }
    if (this.vehicle && this.vehicle.sale_date && this.vehicle.sale_date.date) {
      rv = this.vehicle.sale_date.date.getTime() - this.vehicle.purchase_date.date.getTime();
    }

    return rv;
  }


  public get timeInUseDays(): number {
    let rv = 0;
    const msecPerMinute = 1000 * 60;
    const msecPerHour = msecPerMinute * 60;
    const msecPerDay = msecPerHour * 24;
    rv = Math.ceil(this.timeInUseMs / msecPerDay);

    return rv;
  }

  public get timeInUsePct(): number {
    let rv = 0;
    if (this.maxTimeInUseMls > 0) {
      rv = Math.ceil((this.timeInUseMs / this.maxTimeInUseMls) * 100);
    }
    return rv;
  }

  public progressbarType(pct: number, positive: boolean = true): string {
    let rv = 'success';
    if (pct === 100) {
      rv = positive ? 'info' : 'danger';
    } else if (pct > 50) {
      rv = positive ? 'success' : 'warning';
    } else if (pct > 20) {
      rv = positive ? 'warning' : 'success';
    } else {
      rv = positive ? 'danger' : 'info';
    }
    return rv;
  }


  public get fuelUsedPct(): number {
    let rv = 0;
    if (this.maxTotalFuel > 0 &&
      this.vehicle.vehicles_stat && this.vehicle.vehicles_stat.total_gas_quality) {
      rv = Math.ceil((this.vehicle.vehicles_stat.total_gas_quality / this.maxTotalFuel) * 100);
    }
    return rv;
  }

  public get totalExpencesPct(): number {
    let rv = 0;
    if (this.maxTotalExpences > 0 &&
      this.vehicle.vehicles_stat && this.vehicle.vehicles_stat.total_expences) {
      rv = Math.ceil((this.vehicle.vehicles_stat.total_expences / this.maxTotalExpences) * 100);
    }
    return rv;
  }

  public get timeInUse(): any {
    let days = 0;
    let years = 0;
    let months = 0;

    if (this.timeInUseDays > 0) {
      years = Math.floor(this.timeInUseDays / 365);
      const dm = this.timeInUseDays - (years * 365);
      months = Math.floor(dm / 30);
      days = dm - (months * 30);
    }
    return { y: years, m: months, d: days };
  }

  private getImageClass(): string {
    // tslint:disable-next-line:prefer-const
    let image = new Image();
    if (this.vehicle && this.vehicle.image) {
      image.src = this.imageUri;
    } else {
      image.src = this.imageNotAvailable;
    }

    if (image.width > image.height + 20 || image.width === image.height) {
      // return wide image class
      return Wide;
    } else {
      return Tall;
    }
  }

  public get lastFillUpQuantity(): number {
    let rv = 0;
    // console.dir(this.vehicle);
    if (this.vehicle && this.vehicle.vehicles_stat && this.vehicle.vehicles_stat.gas_quantity) {
      rv = this.vehicle.vehicles_stat.gas_quantity;
    }

    return rv;
  }

  public get lastFillUpTotalPrice(): number {
    let rv = 0;
    if (this.vehicle && this.vehicle.vehicles_stat && this.vehicle.vehicles_stat.gas_total_price) {
      rv = this.vehicle.vehicles_stat.gas_total_price;
    }

    return rv;
  }

  public get lastFillUpPrice(): number {
    let rv = 0;
    if (this.vehicle && this.vehicle.vehicles_stat && this.vehicle.vehicles_stat.gas_cal_price_per_l) {
      rv = this.vehicle.vehicles_stat.gas_cal_price_per_l;
    }

    return rv;
  }

  public get lastFillUpDate(): Date {
    let rv;
    if (this.vehicle && this.vehicle.vehicles_stat &&
      this.vehicle.vehicles_stat.gas_date) {

      rv = new Date(this.vehicle.vehicles_stat.gas_date);
    }

    return rv;
  }


  public get avgGasPrice(): number {
    let rv = 0;
    if (this.vehicle && this.vehicle.vehicles_stat && this.vehicle.vehicles_stat.avg_gas_price) {
      rv = this.vehicle.vehicles_stat.avg_gas_price;
    }

    return rv;
  }

  public get totalGasQuantity(): number {
    let rv = 0;
    if (this.vehicle && this.vehicle.vehicles_stat && this.vehicle.vehicles_stat.total_gas_quality) {
      rv = this.vehicle.vehicles_stat.total_gas_quality;
    }

    return rv;
  }

  public get totalGasPrice(): number {
    let rv = 0;
    if (this.vehicle && this.vehicle.vehicles_stat && this.vehicle.vehicles_stat.total_gas_expences) {
      rv = this.vehicle.vehicles_stat.total_gas_expences;
    }

    return rv;
  }

  public get expencesTotalPrice(): number {
    let rv = 0;
    if (this.vehicle && this.vehicle.vehicles_stat && this.vehicle.vehicles_stat.total_expences) {
      rv = this.vehicle.vehicles_stat.total_expences;
    }

    return rv;
  }

  public get expLastlPrice(): number {
    let rv = 0;
    if (this.vehicle && this.vehicle.vehicles_stat && this.vehicle.vehicles_stat.exp_price) {
      rv = this.vehicle.vehicles_stat.exp_price;
    }

    return rv;
  }

  public get expLastlNote(): string {
    let rv;
    if (this.vehicle && this.vehicle.vehicles_stat && this.vehicle.vehicles_stat.exp_note) {
      if (this.vehicle.vehicles_stat.exp_note.length > 25) {
        rv = this.vehicle.vehicles_stat.exp_note.substr(0, 25) + ' ...';
      } else {
        rv = this.vehicle.vehicles_stat.exp_note;
      }

    }

    return rv;
  }

  public get expLastlDate(): Date {
    let rv;
    if (this.vehicle && this.vehicle.vehicles_stat &&
      this.vehicle.vehicles_stat.exp_date) {

      rv = new Date(this.vehicle.vehicles_stat.exp_date);
    }

    return rv;
  }


  public get locale(): string {
    return this.vehicle.currency_lbl.replace('_', '-');
  }

  public get currencyCode(): string {
    let rv = this.vehicle.currency_lbl;
    switch (this.locale) {
      case 'bg-BG':
        rv = 'BGN';
        break;
      case 'en-GB':
        rv = 'GBP';
        break;
      case 'de-DE':
        rv = 'EUR';
        break;
      default:
        rv = 'USD';
        break;
    }
    return rv;
  }

  public get currencySymbol(): string {
    const rv = getLocaleCurrencyName(this.locale);
    console.log('currencySymbol');
    console.dir(rv);
    return rv;
  }
  public getPrice(val: number): string {
    return new Intl.NumberFormat(this.locale, { style: 'currency', currency: this.currencyCode, useGrouping: true }).format(val);
  }

  //#endregion

  //#region  Events

  onImageLoad() {
    this.imageClass = this.getImageClass();
  }

  /**
  * Card Actions
  */

  hoverOnCard() {
    this.hoverState = Animate.hover;
  }

  hoverOffCard() {
    this.hoverState = Animate.noHover;
  }

  //#endregion

  //#region  Modal

  quickViewClick(vehicle: VehicleModel): void {
    /*
    let qvDto = new QuickViewModelDto();
    qvDto.itemId = productItem.itemId;
    qvDto.catalog = productItem.catalogNumber;
    qvDto.manufacturer = productItem.manufacturerName;
    qvDto.image = productItem.imageUri;
    qvDto.pik = productItem.pik;
    //qvDto.isAmerican = productItem.isAmerican;
    qvDto.productName = productItem.productName;
    qvDto.upc = productItem.upc;
    qvDto.description = productItem.description;
    //Check to make sure item has Col3 pricing.
    if (productItem.pricing) {
      qvDto.price = this.positionedPriceSet.positionOne.value;
      qvDto.priceTypeModel = this.positionedPriceSet.positionOne.priceTypeModel;
      qvDto.priceUom = '/each';
    }
    // Pass 1.Modal Component 2.Data 3.Modal config optins.
    this.dialogService.addDialog(QuickViewModal, qvDto, this.NgbModalOptions).subscribe(
      resp => {
        if(resp === 'addToCart'){
          this.shoppingCartService.addItemToCart(this.productItem);
        }
      }
    ); */
  }

  //#endregion

}
