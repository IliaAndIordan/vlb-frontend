import { Component, EventEmitter, Input, Output, OnInit } from '@angular/core';

@Component({
  selector: 'app-vehicle-card-millage',
  templateUrl: './vehicle-card-millage.component.html',
  styleUrls: ['./vehicle-card-millage.component.css']
})
export class VehicleCardMillageComponent {
  /////////////////
  // CONSTRUCTOR //
  /////////////////
  constructor() { }

  //////////////
  // BINDINGS //
  //////////////
  @Input() millage_current: number;

  ////////////
  // FIELDS //
  ////////////
  getMillageNum(pos: number): number {
    let rv = pos;

    if (this.millage_current) {
      const millage: string = this.padLeft(this.millage_current.toString(), '0', 7);
        rv = parseInt((millage.split(''))[pos], 10);
    }
    return rv;
  }

  padLeft(text: string, padChar: string, size: number): string {
    return (String(padChar).repeat(size) + text).substr((size * -1), size);
  }

}
