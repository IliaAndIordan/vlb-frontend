import { Injectable } from '@angular/core';
import { ServiceNames, ServiceUrl } from '../const/service.names.const';
import { HttpHeaders } from '@angular/common/http';
// Service
import { TokenService } from '../auth/token.service';
import { AuthService } from '../auth/api/auth.service';
import { MessageService } from '../message/message.service';


@Injectable()
export class ApiHelper {

    public ServiceNames = ServiceNames;
    public ServiceUrl = ServiceUrl;

    constructor(private tokenService: TokenService,
        private authService: AuthService,
        private messageService: MessageService) { }

    /**
     * @method getServiceUrl
     * @param serviceName
     * @returns {string}
     */
    public getServiceUrl(serviceName: string): string {
        let retValue = null;
        this.refreshToken();
        const sn = ServiceNames[serviceName];
        // console.log('sn: ' + sn + ' for serviceName:' + serviceName);
        const urlFilter = ServiceUrl.filter(x => x.name === serviceName);
        // console.log('urlFilter.length:' + urlFilter.length);
        if (urlFilter && urlFilter.length > 0) {
            retValue = urlFilter[0].url;
        }
        console.log('sn: ' + sn + ' for erviceUrl:' + retValue);
        return retValue;
    }

    private refreshToken(): void {
        if (this.tokenService.isTokenExpired()) {
            console.log('try to refresh token...');
            this.authService.refreshToken()
                .subscribe(res => {
                    console.dir(res);
                    this.messageService.info('Tocken Refreshed', 'Api Helper');
                });
        }
    }


}
