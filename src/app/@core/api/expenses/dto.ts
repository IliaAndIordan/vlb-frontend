import { VehicleModel, IVehicleModel } from '../vehicle/dto';

//#region Expences

export class VehicleExpenceModel {
    public exp_id: number;
    public vehicle_id: number;
    public exp_date: Date;

    public exp_millage: number;
    public exp_note: string;
    public exp_price: number;
    public currency_lbl: string;
    public track_id: number;

    public static fromJSON(json: IVehicleExpenceModel): VehicleExpenceModel {
        const vs = Object.create(VehicleExpenceModel.prototype);
        return Object.assign(vs, json, {
            exp_date: json.exp_date ? Date.parse(json.exp_date) : undefined,
        });
    }

    // reviver can be passed as the second parameter to JSON.parse
    // to automatically call User.fromJSON on the resulting value.
    public static reviver(key: string, value: any): any {
        return key === '' ? VehicleExpenceModel.fromJSON(value) : value;
    }

    public toJSON(): IVehicleExpenceModel {
        return Object.assign({}, this, {
            exp_date: this.exp_date instanceof Date ?
                new Date(this.exp_date.getTime() -
                    (this.exp_date.getTimezoneOffset() * 60000)).toISOString()
                    .slice(0, 19).replace('T', ' ') : undefined,
        });
    }
}

export interface IVehicleExpenceModel {
    exp_id: number;
    vehicle_id: number;
    exp_date: string;

    exp_millage: number;
    exp_note: string;
    exp_price: number;
    currency_lbl: string;
    track_id: number;
}


//#endregion

//#region Responce / Request

export interface IResponseExpenceData {
    vehicle: IVehicleModel;
    userId: number;
    vehicle_expences: IVehicleExpenceModel[];
}

export interface ResponseVehicleExpences {
    status: string;
    message: string;
    data: IResponseExpenceData;
}

//#endregion

