import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { catchError, map, tap } from 'rxjs/operators';
// Services
import { ApiHelper } from '../api.helper';
import { MessageService } from '../../message/message.service';
import { CurrentUserService } from '../../auth/current-user.service';
// Modelss
import { VehicleExpenceModel } from './dto';
import { TokenService } from '../../auth/token.service';
import { ServiceNames } from '../../const/service.names.const';
import { IEditExpenceModel } from '../../../vehicles/modals/edit-expense.component';

@Injectable()
export class ExpenseService {

  constructor(private apiHelper: ApiHelper,
    private http: HttpClient,
    private messageService: MessageService,
    private currentUserService: CurrentUserService,
    private tokenService: TokenService) { }

  //#region URL Methods

  private get expensesUrl(): string {
    return this.apiHelper.getServiceUrl(ServiceNames.expenses);
  }

  private get expenseUrl(): string {
    return this.apiHelper.getServiceUrl(ServiceNames.expense);
  }

  //#endregion

  getExpences(id: number): Observable<any> {
    // this.messageService.info('Fetching vehicles.', 'Vehicle Service');

    const hdrs = new HttpHeaders({ 'Content-Type': 'application/json' });
    return this.http.post(this.expensesUrl, {vehicleId: id}, { headers: hdrs } )
      .pipe(
        tap(res => { this.log(`Vehicle expences fetched.`);  }),
        catchError(this.handleError('getVehicleExpences', []))
      );
  }

  getExpense(id: number): Observable<any> {
    // this.messageService.info('Fetching vehicles.', 'Vehicle Service');

    const hdrs = new HttpHeaders({ 'Content-Type': 'application/json' });
    return this.http.post(this.expenseUrl, {vehicleId: id}, { headers: hdrs } )
      .pipe(
        tap(res => { this.log(`Vehicle fetched.`); }),
        catchError(this.handleError('getVehicle', []))
      );
  }

  updateExpense(exp: VehicleExpenceModel): Observable<any> {
    // this.messageService.info('Fetching vehicles.', 'Vehicle Service');
    console.dir(exp);
    const hdrs = new HttpHeaders({ 'Content-Type': 'application/json' });
    return this.http.post(this.expenseUrl, {expense: exp}, { headers: hdrs } )
      .pipe(
        tap(res => { this.log(`Expense updated.`); }),
        catchError(this.handleError('updateExpense', []))
      );
  }


  addExpense(exp: VehicleExpenceModel): Observable<any> {
    // this.messageService.info('Fetching vehicles.', 'Vehicle Service');
    console.dir(exp);
    const hdrs = new HttpHeaders({ 'Content-Type': 'application/json' });
    return this.http.put(this.expenseUrl, {expense: exp}, { headers: hdrs } )
      .pipe(
        tap(res => { this.log(`Expense added.`); }),
        catchError(this.handleError('addExpense', []))
      );
  }
   //#region Base

  /**
* Handle Http operation that failed.
* Let the app continue.
* @param operation - name of the operation that failed
* @param result - optional value to return as the observable result
*/
private handleError<T>(operation = 'operation', result?: T) {
  return (error: any): Observable<T> => {

    // TODO: send the error to remote logging infrastructure
    console.error(error); // log to console instead

    // TODO: better job of transforming error for user consumption
    this.messageService.error(`${operation} failed: ${error.message}`, 'Expense Service');

    // Let the app keep running by returning an empty result.
    return of(result as T);
  };
}

/** Log a HeroService message with the MessageService */
private log(message: string) {
  this.messageService.info(message, 'Expense Service');
}

//#endregion

}
