import { VehicleModel, IVehicleModel } from '../vehicle/dto';
import { GasBrand } from '../../const/gas-brand';

//#region Gas

export class VehicleGasModel {
    public gas_id: number;
    public vehicle_id: number;
    public gas_date: Date;
    public gas_millage: number;
    public gas_quantity: number;
    public gas_total_price: number;
    public currency_lbl: string;
    public gas_location_id: number;
    public location_brand: string;

    public cal_fule_consumption: number;
    public cal_price_per_l: number;
    // From Statistics
    public avg_gas_price: number;
    public fule_economy: number;

    public static fromJSON(json: IVehicleGasModel): VehicleGasModel {
        // console.log('fromJSON', json);
        const vs = Object.create(VehicleGasModel.prototype);
        return Object.assign(vs, json, {
            gas_date: json.gas_date ? Date.parse(json.gas_date) : undefined,
        });
    }

    // reviver can be passed as the second parameter to JSON.parse
    // to automatically call User.fromJSON on the resulting value.
    public static reviver(key: string, value: any): any {
        return key === '' ? VehicleGasModel.fromJSON(value) : value;
    }

    public toJSON(): IVehicleGasModel {
        // console.log('toJSON', this);
        return Object.assign({}, this, {
            gas_date: this.gas_date instanceof Date ?
                new Date(this.gas_date.getTime() -
                    (this.gas_date.getTimezoneOffset() * 60000)).toISOString()
                    .slice(0, 19).replace('T', ' ') : undefined,

        });
    }

    public get gasBrandImageUrl(): string {
        let retValue = 'unknown.png';
        const filter = GasBrand.filter(x => x.name === this.location_brand);

        if (filter && filter.length > 0) {
            retValue = filter[0].image;
        }

        return 'assets/img/gasbrand/' + retValue;
    }

    public get priceDiff(): number {
        const retValue =  this.cal_price_per_l - this.avg_gas_price;

        return retValue;
    }

    public get priceDiffIconArrow(): string {
        let retValue = 'fa fa-arrow-down down';
        if (this.priceDiff > 0) {
            retValue = 'fa fa-arrow-up up';
        } else if (this.priceDiff === 0) {
            retValue = 'fa fa-dot-circle-o';
        }
        return retValue;
    }
}

export interface IVehicleGasModel {
    gas_id: number;
    vehicle_id: number;
    gas_date: string;
    gas_millage: number;
    gas_quantity: number;
    gas_total_price: number;
    currency_lbl: string;
    gas_location_id: number;
    location_brand: string;

    cal_fule_consumption: number;
    cal_price_per_l: number;
    // From Stat
    avg_gas_price: number;
    fule_economy: number;
}


export class FuelRetailer {
    name: string;
    image: string;

    constructor(name: string) {
        this.name = name;
        const filter = GasBrand.filter(x => x.name === this.name);

        if (filter && filter.length > 0) {
            this.image = filter[0].image;
        } else {
            this.image = 'unknown.png';
        }
    }

    public get imageUrl(): string {
        let retValue = 'unknown.png';
        const filter = GasBrand.filter(x => x.name === this.name);

        if (filter && filter.length > 0) {
            retValue = filter[0].image;
        }

        return 'assets/img/gasbrand/' + retValue;
    }

    public static getFuelRetailers(): FuelRetailer[] {
        const retValue: FuelRetailer[] = [];
        if (GasBrand && GasBrand.length > 0) {
            for (let idx = 0; idx < GasBrand.length; idx++) {
                const element = GasBrand[idx];
                const fr = new FuelRetailer(element.name);
                fr.image = element.image;
                retValue.push(fr);


            }
        }
        return retValue;
    }
}
//#endregion


//#region Responce / Request

export interface IResponseGasLoadData {
    vehicle_id: number;
    userId: number;
    vehicle_gas_all: IVehicleGasModel[];
}

export interface ResponseVehicleGasLoad {
    status: string;
    message: string;
    data: IResponseGasLoadData;
}

//#endregion

