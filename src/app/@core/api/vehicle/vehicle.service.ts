import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { catchError, map, tap } from 'rxjs/operators';
// Services
import { ApiHelper } from '../api.helper';
import { MessageService } from '../../message/message.service';
import { CurrentUserService } from '../../auth/current-user.service';
// Modelss
import { VehicleModel, ResponseVehicles, IResponseVehiclesData } from './dto';
import { TokenService } from '../../auth/token.service';
import { ServiceNames } from '../../const/service.names.const';

@Injectable()
export class VehicleService {


  constructor(private apiHelper: ApiHelper,
    private http: HttpClient,
    private messageService: MessageService,
    private currentUserService: CurrentUserService,
  private tokenService: TokenService) { }

  //#region URL Methods

  private get vehiclesUrl(): string {
    return this.apiHelper.getServiceUrl(ServiceNames.vehicles);
  }

  private get vehicleUrl(): string {
    return this.apiHelper.getServiceUrl(ServiceNames.vehicle);
  }

  private get expensesUrl(): string {
    return this.apiHelper.getServiceUrl(ServiceNames.expenses);
  }

  private get gasloadUrl(): string {
    return this.apiHelper.getServiceUrl(ServiceNames.gasload);
  }

  private get vehicleEditUrl(): string {
    return this.apiHelper.getServiceUrl(ServiceNames.vehicleedit);
  }

  //#endregion

  getVehicles(): Observable<any> {
    // this.messageService.info('Fetching vehicles.', 'Vehicle Service');

    const hdrs = new HttpHeaders({ 'Content-Type': 'application/json' });
    return this.http.post(this.vehiclesUrl, {text: 'test'}, { headers: hdrs } )
      .pipe(
        tap(res => { this.log(`Vehicles fetched.`); console.dir(res); }),
        catchError(this.handleError('getVehicles', []))
      );
  }

  getVehicle(id: number): Observable<any> {
    // this.messageService.info('Fetching vehicles.', 'Vehicle Service');

    const hdrs = new HttpHeaders({ 'Content-Type': 'application/json' });
    return this.http.post(this.vehicleUrl, {vehicleId: id}, { headers: hdrs } )
      .pipe(
        tap(res => { this.log(`Vehicle fetched.`); }),
        catchError(this.handleError('getVehicle', []))
      );
  }

  getVehicleExpences(id: number): Observable<any> {
    // this.messageService.info('Fetching vehicles.', 'Vehicle Service');

    const hdrs = new HttpHeaders({ 'Content-Type': 'application/json' });
    return this.http.post(this.expensesUrl, {vehicleId: id}, { headers: hdrs } )
      .pipe(
        tap(res => { this.log(`Vehicle expences fetched.`);  }),
        catchError(this.handleError('getVehicleExpences', []))
      );
  }


  editVehicle(vm: VehicleModel): Observable<any> {
    // this.messageService.info('Fetching vehicles.', 'Vehicle Service');

    const hdrs = new HttpHeaders({ 'Content-Type': 'application/json' });
    return this.http.post(this.vehicleEditUrl, {vehicle: vm}, { headers: hdrs } )
      .pipe(
        tap(res => { this.log(`Vehicle saved.`);  }),
        catchError(this.handleError('editVehicle', []))
      );
  }

  newVehicle(vm: VehicleModel): Observable<any> {
    // this.messageService.info('Fetching vehicles.', 'Vehicle Service');

    const hdrs = new HttpHeaders({ 'Content-Type': 'application/json' });
    return this.http.put(this.vehicleEditUrl, {vehicle: vm}, { headers: hdrs } )
      .pipe(
        tap(res => { this.log(`Vehicle created.`);  }),
        catchError(this.handleError('editVehicle', []))
      );
  }

  deleteVehicle(id: number): Observable<any> {
    // this.messageService.info('Fetching vehicles.', 'Vehicle Service');
    const hdrs = new HttpHeaders({ 'Content-Type': 'application/json' });
    const url = `${this.vehicleEditUrl}/${id}`;
    return this.http.delete(url, { headers: hdrs } )
      .pipe(
        tap(res => { this.log(`Vehicle deleted.`);  }),
        catchError(this.handleError('deleteVehicle', []))
      );
  }

  //#region Base

  /**
* Handle Http operation that failed.
* Let the app continue.
* @param operation - name of the operation that failed
* @param result - optional value to return as the observable result
*/
  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // TODO: better job of transforming error for user consumption
      this.messageService.error(`${operation} failed: ${error.message}`, 'Vehicle Service');

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }

  /** Log a HeroService message with the MessageService */
  private log(message: string) {
    this.messageService.info(message, 'Vehicle Service');
  }

  //#endregion

}
