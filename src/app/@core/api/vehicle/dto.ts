


export class VehicleModel {

    public vehicleId: number;
    public user_id: number;
    public name: string;

    public first_reg: DateModel;
    public model: string;
    public milage_km_start: number;
    public milage_km_current: number;
    public image: string;
    public purchase_date: DateModel;
    public track_in_mi: boolean;
    public currency_lbl: string;
    public sale_date: DateModel;

    public vehicles_stat: VehicleStat;

    public timeInUseMs: number;

    public static fromJSON(json: IVehicleModel): VehicleModel {
        const vs = Object.create(VehicleModel.prototype);
        return Object.assign(vs, json, {
            first_reg: DateModel.fromJSON(json.first_reg),
            sale_date: DateModel.fromJSON(json.sale_date),
            purchase_date: DateModel.fromJSON(json.purchase_date),
        });
    }

    // reviver can be passed as the second parameter to JSON.parse
    // to automatically call User.fromJSON on the resulting value.
    public static reviver(key: string, value: any): any {
        return key === '' ? VehicleModel.fromJSON(value) : value;
    }

    public toJSON(): IVehicleModel {
        return Object.assign({}, this, {
            first_reg: this.first_reg ? this.first_reg.toJSON() : undefined,
            purchase_date: this.purchase_date ? this.purchase_date.toJSON() : undefined,
            sale_date: this.sale_date ? this.sale_date.toJSON() : undefined
        });
    }

    //#region  Utility

    public getTimeInUseDays(): number {
        let rv = 0;
        // console.log('getTimeInUseDays->');
        const msecPerMinute = 1000 * 60;
        const msecPerHour = msecPerMinute * 60;
        const msecPerDay = msecPerHour * 24;
        rv = Math.ceil(this.timeInUseMs / msecPerDay);
        // console.log('getTimeInUseDays-> rv:' + rv);
        return rv;
    }

    public getTimeInUseYmd(): any {
        let days = 0;
        let years = 0;
        let months = 0;
        // console.log('getTimeInUseYmd->');
        if (this.getTimeInUseDays() > 0) {
            years = Math.floor(this.getTimeInUseDays() / 365);
            const dm = this.getTimeInUseDays() - (years * 365);
            months = Math.floor(dm / 30);
            days = dm - (months * 30);
        }
        // console.log('getTimeInUseYmd<- days: ' + days);
        return { y: years, m: months, d: days };
    }

    public calcTimeInUseMs(): number {
        let rv = 0;

        if (this.purchase_date && this.purchase_date.date) {
            rv = new Date().getTime() - this.purchase_date.date.getTime();
        }
        if (this.sale_date && this.sale_date.date) {
            rv = this.sale_date.date.getTime() - this.purchase_date.date.getTime();
        }

        return rv;
    }

    public getGasMeasure(): string {
        let retValue = 'L';

        if (this.track_in_mi) {
            retValue = 'gl';
        }
        return retValue;
    }

    //#endregion
}

export class VehicleStat {

    //#region Last Fuel Up

    public last_fillup_id: number;
    public gas_date: Date;
    public gas_quantity: number;
    public gas_total_price: number;
    public gas_cal_price_per_l: number;
    // Averige Gas Price (all records)
    public avg_gas_price: number;

    // Stat fields
    // 6L/100 km 0r 6Gal/100 mi
    public fule_economy: number;
    // total amount spend on fuel
    public total_gas_expences: number;

    public total_gas_quality: number;
    //#endregion

    //#region Last Expence
    public last_expence_id: number;
    public exp_date: Date;
    public exp_note: string;
    public exp_price: number;
    // Stat Total cost of ownnership
    public total_expences: number;

    //#endregion

    public last_trip_id: number;
    public total_trip_distance: number;
    public vehicle_id: number;

    public static fromJSON(json: IVehicleStat): VehicleStat {
        const vs = Object.create(VehicleStat.prototype);
        return Object.assign(vs, json, {
            exp_date: json.exp_date ? Date.parse(json.exp_date) : undefined,
            gas_date: json.gas_date ? Date.parse(json.gas_date) : undefined
        });
    }

    // reviver can be passed as the second parameter to JSON.parse
    // to automatically call User.fromJSON on the resulting value.
    public static reviver(key: string, value: any): any {
        return key === '' ? VehicleStat.fromJSON(value) : value;
    }

    public toJSON(): IVehicleStat {
        return Object.assign({}, this, {
            // Date().toISOString().slice(0, 19).replace('T', ' ');
            exp_date: this.exp_date instanceof Date ? this.exp_date.toISOString().slice(0, 19).replace('T', ' ') : undefined,
            gas_date: this.gas_date instanceof Date ? this.gas_date.toISOString().slice(0, 19).replace('T', ' ') : undefined
        });
    }
}

//#region  Interfaces

export interface IVehicleStat {

    //#region Last Fuel Up

    last_fillup_id: number;
    gas_date: string;
    gas_quantity: number;
    gas_total_price: number;
    gas_cal_price_per_l: number;
    // Averige Gas Price (all records)
    avg_gas_price: number;

    // Stat fields
    // 6L/100 km 0r 6Gal/100 mi
    fule_economy: number;
    // total amount spend on fuel
    total_gas_expences: number;

    total_gas_quality: number;
    //#endregion

    //#region Last Expence
    last_expence_id: number;
    exp_date: string;
    exp_note: string;
    exp_price: number;
    // Stat Total cost of ownnership
    total_expences: number;

    //#endregion

    last_trip_id: number;
    total_trip_distance: number;
    vehicle_id: number;
}

export interface IVehicleModel {

    vehicleId: number;
    user_id: number;
    name: string;

    first_reg: IDateModel;
    model: string;
    milage_km_start: number;
    milage_km_current: number;
    image: string;
    purchase_date: IDateModel;
    track_in_mi: boolean;
    currency_lbl: string;
    sale_date: IDateModel;
}

export class DateModel {
    date: Date;
    timezone: string;
    timezone_type: number;

    public static fromJSON(json: IDateModel): DateModel {
        const dm = Object.create(DateModel.prototype);
        return Object.assign(dm, json, {
            date: (json && json.date) ? new Date(json.date) : undefined
        });
    }

    // reviver can be passed as the second parameter to JSON.parse
    // to automatically call User.fromJSON on the resulting value.
    public static reviver(key: string, value: any): any {
        return key === '' ? DateModel.fromJSON(value) : value;
    }

    public toJSON(): IDateModel {
        const tzoffset = (new Date()).getTimezoneOffset() * 60000;
        return Object.assign({}, this, {
            date: this.date ? new Date(this.date.getTime() -
                (this.date.getTimezoneOffset() * 60000)).toISOString()
                .slice(0, 19).replace('T', ' ') : undefined
        });
    }
}

export interface IDateModel {
    date: string;
    timezone: string;
    timezone_type: number;
}

//#endregion

//#region Responce Interfaces

export interface IResponseVehiclesData {
    vehicles: IVehicleModel[];
    userId: number;
    vehicles_stat: IVehicleStat[];
}

export interface ResponseVehicles {
    status: string;
    message: string;
    data: IResponseVehiclesData;
}

export interface IResponseVehicleData {
    vehicle: IVehicleModel;
    userId: number;
    vehicle_stat: IVehicleStat;
}

export interface ResponseVehicle {
    status: string;
    message: string;
    data: IResponseVehicleData;
}

//#endregion
