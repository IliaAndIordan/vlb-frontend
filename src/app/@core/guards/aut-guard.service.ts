import { Injectable } from '@angular/core';
import { Router, Route, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, CanActivateChild, CanLoad } from '@angular/router';
import { AppRoutes } from '../const/routes/app-routes.const';
import { CurrentUserService } from '../auth/current-user.service';
import { TokenService } from '../auth/token.service';

@Injectable()
export class AutGuard implements CanActivate, CanActivateChild, CanLoad {

  constructor(private router: Router,
    private currentUserService: CurrentUserService,
    private tokenService: TokenService) { }

  canLoad(route: Route) {
    return true;
  }

  canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    if (this.currentUserService.isAuthenticated) {
      return true;
    }

    this.router.navigate(['/', AppRoutes.Login], { queryParams: { redirectTo: state.url } });
    return false;
  }

  canActivateChild(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    return this.canActivate(route, state);
  }
}
