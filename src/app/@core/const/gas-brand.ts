export const GasBrand = [
    {
        name: 'eko',
        image: 'eko.jpg'
    },
    {
        name: 'omv',
        image: 'omv.png'
    },
    {
        name: 'petrol',
        image: 'petrol.png'
    },
    {
        name: 'shell',
        image: 'shell.png'
    },
    {
        name: 'unknown',
        image: 'unknown.png'
    },
];

