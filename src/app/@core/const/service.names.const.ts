export const ServiceUrl = [
    {
        name: 'autenticate',
        url: 'http://ws.vlb.iordanov.info/login'
    },
    {
        name: 'refreshToken',
        url: 'http://ws.vlb.iordanov.info/jwtrefresh'
    },
    {
        name: 'vehicles',
        url: 'http://ws.vlb.iordanov.info/vehicles'
    },
    {
        name: 'vehicle',
        url: 'http://ws.vlb.iordanov.info/vehicle'
    },
    {
        name: 'ml',
        url: 'http://ws.vlb.iordanov.info/ml'
    },
    {
        name: 'mlload',
        url: 'http://ws.vlb.iordanov.info/mlload'
    },
    {
        name: 'expenses',
        url: 'http://ws.vlb.iordanov.info/expenses'
    },
    {
        name: 'expense',
        url: 'http://ws.vlb.iordanov.info/expense'
    },
    {
        name: 'gasload',
        url: 'http://ws.vlb.iordanov.info/gasload'
    },
    {
        name: 'gas',
        url: 'http://ws.vlb.iordanov.info/gas'
    },
    {
        name: 'vehicleedit',
        url: 'http://ws.vlb.iordanov.info/vehicleedit'
    },
];

export const ServiceNames = {
    autenticate: 'autenticate',
    refreshToken: 'refreshToken',
    vehicles: 'vehicles',
    vehicle: 'vehicle',
    mlload: 'mlload',
    ml: 'ml',
    expenses: 'expenses',
    expense: 'expense',
    gasload: 'gasload',
    gas: 'gas',
    vehicleedit: 'vehicleedit'
};
