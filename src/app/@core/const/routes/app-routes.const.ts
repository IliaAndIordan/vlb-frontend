export const AppRoutes = {
    Root: '',
    Home: 'home',
    Login: 'login',
    Dashboard: 'dashboard',
    NotAuthorized: 'not-authorized',
    UserProfile: 'user-profile',
    Vehicles: 'vehicles',
    Vehicle: 'vehicle',
};
