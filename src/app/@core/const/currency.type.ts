export const CurrencyType = {
    bg_BG: 'BGN',
    en_GB: 'GBP',
    de_DE: 'EUR',
    en_US: 'USD',
};

