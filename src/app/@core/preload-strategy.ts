import { Route, PreloadingStrategy } from '@angular/router';
import { Observable } from 'rxjs/Observable';

// add data:{preload:true} to lazy loaded module
export class PreloadSelectedModuleList implements PreloadingStrategy {
    preload(route: Route, load: Function): Observable<any> {
        return route.data && route.data.preload ? load() : null;
    }
}
