import { Injectable } from '@angular/core';
import { HttpClient, HttpRequest, HttpParams, HttpResponse, HttpHeaders, HttpHeaderResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { catchError, map, tap } from 'rxjs/operators';
import { of } from 'rxjs/observable/of';

// Services
import { ApiHelper } from '../../api/api.helper';
import { MessageService } from '../../message/message.service';
import { ServiceNames } from '../../const/service.names.const';


export class MlLabel {
  KEY: string;
  value: string;
}

export class MlLabelsModel {
  labels: MlLabel[];
  language: string;

  public get translations() {
    const arr = [];
    if (this.labels) {
      for (let idx = 0; idx < this.labels.length; idx++) {
        const element = this.labels[idx];
        arr[ element.KEY] = element.value;
      }
    }
    // console.dir(arr);
    return arr;
  }
}

export class ResponseMlLoad {
  status: string;
  message: string;
  data: MlLabelsModel;
}


@Injectable()
export class MlService {

  constructor(
    private http: HttpClient,
    private messageService: MessageService) { }

  private get mlUrl(): string {
    return 'http://ws.vlb.iordanov.info/ml';
  }

  private get mlLoadUrl(): string {
    return 'http://ws.vlb.iordanov.info/mlload';
  }

  public mlLoad(language: string): Observable<MlLabelsModel> {
    // this.messageService.info('Fetching translation' + language + '.', 'ML Service');

    const hdrs = new HttpHeaders({ 'Content-Type': 'application/json' });
    return this.http.post<ResponseMlLoad>(this.mlLoadUrl, { language: language }, { headers: hdrs })
      .pipe(
        map((res: ResponseMlLoad) => this.mlLoadTransform(res)),
        tap(labels => { this.log(`Translation fetched.`); console.dir(labels); }),
        catchError(this.handleError<MlLabelsModel>('mlLoad'))
      );
  }

  private mlLoadTransform(response: ResponseMlLoad): MlLabelsModel {
    console.dir(response);
    const rv: MlLabelsModel = new MlLabelsModel();
    rv.language = response.data.language;
    rv.labels = [];
    for (let idx = 0; idx < response.data.labels.length; idx++) {
      const element = response.data.labels[idx];
      const tmp = Object.assign(new MlLabel(), element);
      rv.labels.push(tmp);
    }

    return rv;
  }

  public mlInsert(language: string, key: string): Observable<ResponseMlLoad> {
    // this.messageService.info('Fetching translation' + language + '.', 'ML Service');

    const hdrs = new HttpHeaders({ 'Content-Type': 'application/json' });
    return this.http.post<ResponseMlLoad>(this.mlUrl, { language: language, key: key }, { headers: hdrs })
      .pipe(
        // map((res: ResponseMlLoad) => this.mlLoadTransform(res)),
        tap(responce => { this.log(`Translation inserted.`); console.dir(responce); }),
        catchError(this.handleError<ResponseMlLoad>('mlInsert'))
      );
  }

  //#region Base

  /**
* Handle Http operation that failed.
* Let the app continue.
* @param operation - name of the operation that failed
* @param result - optional value to return as the observable result
*/
  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // TODO: better job of transforming error for user consumption
      this.messageService.error(`${operation} failed: ${error.message}`, 'ML Service');

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }

  /** Log a HeroService message with the MessageService */
  private log(message: string) {
    this.messageService.info(message, 'ML Service');
  }

  //#endregion
}
