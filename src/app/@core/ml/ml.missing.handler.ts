import { MissingTranslationHandler, MissingTranslationHandlerParams, TranslateService, TranslateLoader } from '@ngx-translate/core';
import { MlService } from './api/ml.service';
import { MlLoader } from './ml.loader';
import { Subscription } from 'rxjs/Subscription';

export class MlMissingHandler implements MissingTranslationHandler {


    public isLabelsLoading: boolean = true;

    constructor(private mlService: MlService) {
    }
    /**
     * handle
     * @param params MissingTranslationHandlerParams which include the key and the translationService.
     */
    handle(params: MissingTranslationHandlerParams) {

        // console.dir(params);
        let label = '';
        if (params && params.key) {

            label = params.key.replace('_', ' ');
            const lang = params.translateService.currentLang;
            const loader: MlLoader = params.translateService.currentLoader as MlLoader;
            if (loader.isKeyExist(params.key, lang)) {
                label = loader.getlabel(params.key);
                // console.log('key: ' + params.key + ', label: ' + label);
                params.translateService.set(params.key, label, lang);
            }

            if (!this.isLabelsLoading) {
                console.log('Missing key: ' + params.key + ', isLabelsLoading:' + this.isLabelsLoading);
                loader.insertLabel(params.key, lang);
                params.translateService.set(params.key, label, lang);
            }
        }
        return label;
    }
}
