import { Http } from '@angular/http';
import { TranslateLoader } from '@ngx-translate/core';
import { Observable } from 'rxjs/observable';
import { of } from 'rxjs/observable/of';
import { Subject } from 'rxjs/Subject';
// Services
import { CurrentUserService } from '../auth/current-user.service';
// Models
import { MlLabelsModel, MlLabel, MlService } from './api/ml.service';
import { Language } from '../const/language.const';

export class MlLoader implements TranslateLoader {

    private labels: Observable<any>;
    public LabelsLoaded = new Subject<string>();
    public LabelsLoading = new Subject<boolean>();
    public isLoadingLabels = false;

    constructor(private http: Http,
        private cus: CurrentUserService,
        private mlService: MlService) { }


    getTranslation(lang: string): Observable<any> {
        this.labels = of([]);
        console.log('MlLoader->getTranslation for ' + lang);
        if (lang) {
            const langChage = lang !== this.cus.language;
            if (lang !== this.cus.language) {
                // this.cus.language = lang;
            }

            if (this.cus.translations && this.cus.translations.language === lang) {
                console.log('MlLoader-> has translations for ' + this.cus.translations.language);
                this.labels = of(this.cus.translations.translations);
            } else {
                console.log('MlLoader->getTranslation isLoadingLabels: ' + this.isLoadingLabels + '.');
                if (!this.isLoadingLabels) {
                    console.log('MlLoader->getTranslation for ' + lang + ' mlService');
                    this.isLoadingLabels = true;
                    this.LabelsLoading.next(this.isLoadingLabels);
                    this.mlService.mlLoad(lang)
                        .subscribe(mlModel => {
                            this.cus.translations = mlModel;
                            this.labels = of(mlModel.translations);
                            console.log('MlLoader->getTranslation LabelsLoaded ' + lang + '.');
                            this.LabelsLoaded.next(lang);
                            this.isLoadingLabels = false;
                            this.LabelsLoading.next(this.isLoadingLabels);

                            return this.labels;
                        });

                }
            }
        }
        console.log('getTranslation(' + lang + ')');
        console.dir(this.labels);
        return this.labels as Observable<any>;
    }

    private mlLoadProcess(mlModel, lang) {
        this.cus.translations = mlModel;
        this.labels = of(mlModel.translations);

        console.log('MlLoader->getTranslation LabelsLoaded ' + lang + '.');
        this.LabelsLoaded.next(lang);
        this.isLoadingLabels = false;

    }
    public insertLabel(key: string, lang: string) {
        if (lang) {

            if (key) {

                console.log('Insert key: ' + key);
                this.mlService.mlInsert(lang, key)
                    .subscribe(response => {
                        if (response) {
                            console.log(response.message);
                            // this.LabelsLoaded.next(lang);
                            // this.isLoadingLabels = false;
                        }
                    });

            }
        }
    }

    public isKeyExist(key: string, lang: string): boolean {
        let retValue = false;
        if (key) {
            if (this.cus.translations && this.cus.translations.language === lang) {
                const label: MlLabel = this.cus.translations.labels.find(x => x.KEY === key);
                retValue = label === undefined;
            }
            // console.log('isKeyExist: ' + key + ' -> ' + retValue);
        }

        return retValue;
    }

    public getlabel(key: string): string {
        let retValue = key.replace('_', ' ');
        if (key) {
            if (this.cus.translations) {
                const label: MlLabel = this.cus.translations.labels.find(x => x.KEY === key);
                if (label) {
                    retValue = label.value;
                }
            }
        }

        return retValue;
    }
}
