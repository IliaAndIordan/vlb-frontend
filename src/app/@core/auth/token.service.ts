import { Injectable, Inject } from '@angular/core';
import { JwtHelperService } from '@auth0/angular-jwt';
import { TokenFactory } from './token-factory';

@Injectable()
export class TokenService extends TokenFactory {


  constructor(private jwtHelper: JwtHelperService) {
    super();
  }

  //#region JwtHelperService methods

  public isTokenExpired(): boolean {
    const retVal = this.jwtHelper.isTokenExpired();
    if (retVal) {
      // console.log('isTokenExpired: ' + retVal);
    }
    return retVal;
  }

  public getTokenExpirationDate(): Date {
    return this.jwtHelper.getTokenExpirationDate();
  }

  public decodeToken(): string {
    return this.jwtHelper.decodeToken();
  }

  //#endregion

  //#region Token

  public set barerToken(value: string) {
    if (!value) {
      return;
    }

    localStorage.setItem(this.__refreshTokenKey, value);
  }

  public get barerToken(): string {
    return localStorage.getItem(this.__refreshTokenKey);
  }

  public clearToken() {
    localStorage.removeItem(this.__refreshTokenKey);
  }

  //#endregion
}
