import { Injectable } from '@angular/core';
import { HttpClient, HttpRequest, HttpParams, HttpResponse, HttpHeaders, HttpHeaderResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { catchError, map, tap } from 'rxjs/operators';
import { of } from 'rxjs/observable/of';


// Services
import { TokenService } from '../token.service';
import { MessageService } from '../../message/message.service';
import { CurrentUserService } from '../current-user.service';

// Models
import { ResponseAuthenticate, UserModel, UserRole } from './dtos';
// import { JwtHelperService } from '@auth0/angular-jwt';


@Injectable()
export class AuthService {

  private baseUrl = 'http://ws.vlb.iordanov.info/';
  private loginUrl = this.baseUrl + 'login';
  private refreshTokenUrl = this.baseUrl + 'jwtrefresh';

  private refreshTokenTimerId: any;

  constructor(
    private client: HttpClient,
    private tokenService: TokenService,
    private messageService: MessageService,
    private currentUserService: CurrentUserService) { }


  autenticate(username: string, password: string): Observable<any> {
    console.log('user: ' + username + ', ped: ' + password);
    const hdrs = new HttpHeaders({ 'Content-Type': 'application/json' });
    return this.client.post<HttpResponse<any>>(
      this.loginUrl,
      { username: username, password: password },
      { headers: hdrs, observe: 'response' })
      .pipe(
        map((responce: HttpResponse<any>) => this.convertAutenticateToUser(responce)),
        /*
        tap((res: HttpResponse<any>) => {
          console.log(res.headers.keys);
        }),*/
        catchError(this.handleError('autenticate', []))
      );
    /*
      .map(user => {
        // login successful if there's a jwt token in the response
        if (user && user.token) {
          // store user details and jwt token in local storage to keep user logged in between page refreshes
          localStorage.setItem('currentUser', JSON.stringify(user));
        }

        return user;
      });
      */
  }

  refreshToken(): Observable<any> {

    console.log('refreshToken: ');
    const hdrs = new HttpHeaders({ 'Content-Type': 'application/json' });
    return this.client.post<HttpResponse<any>>(this.refreshTokenUrl,
      { token: this.tokenService.barerToken },
      { headers: hdrs, observe: 'response' })
      .pipe(
        map((responce: HttpResponse<any>) => this.convertAutenticateToUser(responce)),
        /*
        tap((responce: HttpResponse<any>) => {
          console.dir(responce);
          this.messageService.info('Token Refreshed', 'System Information');
        }),*/
        catchError(this.handleError('refreshToken', []))
      );
  }

  convertAutenticateToUser(response: HttpResponse<any>): UserModel {
    let userObj = null;
    let token = null;
    if (response) {
      if (response.headers) {
        token = response.headers.get('X-Authorization');
      }

      if (response.body) {
        const res = Object.assign(new ResponseAuthenticate(), response.body);
        console.dir(res);
        if (res.data && res.data.current_user) {
          userObj = res.data.current_user; // Object.assign( new ResponseAuthenticate(), res.data.current_user) ;
          console.dir(userObj);
          console.log(' User: ' + userObj.name + ' auth.');
          this.currentUserService.user = userObj;
        } else {
          this.messageService.error(res.message, 'Autentication Failed.');
        }
      }


      if (token) {
        this.tokenService.barerToken = token;
        /*
        if (!this.refreshTokenTimerId) {
          this.refreshTokenTimerId = setInterval(function () {
            if (!this.tockenService.barerToken) {
              clearInterval(this.refreshTokenTimerId);
              this.refreshTokenTimerId = null;
              return;
            }
            console.log(' ---- begin setInterval ----');
            this.refreshToken()
            .subscribe(user => {
              if (user) {
                this.messageService.success('Welcome back ' + user.name + '!', 'Login success');
              } else {
                this.messageService.successr('Login Failed', 'Login Failed');
              }
            });
            console.log(' ---- end setInterval ----');
          }.bind(this), 5000);

        }
        */
        console.log(' ---- decode');
        console.log(this.tokenService.decodeToken());

      }

    }

    // console.dir(retvalue);
    return userObj;
  }

  logout() {
    // remove user from local storage to log user out
    this.tokenService.clearToken();
    this.currentUserService.user = undefined;
  }

  /**
 * Handle Http operation that failed.
 * Let the app continue.
 * @param operation - name of the operation that failed
 * @param result - optional value to return as the observable result
 */
  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // TODO: better job of transforming error for user consumption
      this.messageService.error(`${operation} failed: ${error.message}`, 'Autentication');

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }

  /** Log a  message with the MessageService */
  private log(message: string) {
    this.messageService.info(message, 'Autentication');
  }
}
