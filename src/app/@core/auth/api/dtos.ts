export class UserModel {

    public id: number;
    public eMail: string;
    public name: string;
    // 2 predefined roles 0-user, 1 admin, 2 editor
    public role = 0;
    public roleName: string;
    public isReceiveEMails: boolean;
    public ipAddress: string;
    public lastLogged: Date;
    public updated: Date;
}

export enum UserRole {
    User = 1,
    Editor = 2,
    Admin = 3
}
export class ResponseAuthenticateUser {
    public current_user: UserModel;
}

export class ResponseAuthenticate {
    public status: string;
    public data: ResponseAuthenticateUser;
    public message: string;
}
