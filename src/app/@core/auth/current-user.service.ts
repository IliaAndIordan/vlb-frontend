import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';
import { Subject } from 'rxjs/Subject';
// Services
// import { GravatarService } from '@infinitycube/gravatar';
import { TokenService } from './token.service';
// Models
import { UserModel } from '../auth/api/dtos';
import { MlService, MlLabel, MlLabelsModel } from '../ml/api/ml.service';
import { Subscription } from 'rxjs/Subscription';
import { VehicleModel } from '../api/vehicle/dto';



@Injectable()
export class CurrentUserService {

  private readonly _sessionUserKey = 'session_user_key';

  private readonly _localLanguageKey = 'local_language';
  private readonly _localLanguageLabelsKey = 'local_language_labels';

  private readonly _localVehiclesKey = 'local_vehicles';
  private readonly _localSelectedVehicleKey = 'local_selected_vehicle';

  constructor(private tockenService: TokenService,
    private mlService: MlService,
    /*private gravatar: GravatarService*/) {
  }

  //#region Session User`

  /**
   * User Changed subject
   * subscribe to it if you want to be notified when changes appear
   */
  public UserChanged = new Subject<UserModel>();

  public get user(): UserModel {
    const userStr = sessionStorage.getItem(this._sessionUserKey);
    let userObj = null;
    if (userStr) {
      userObj = Object.assign(new UserModel(), JSON.parse(userStr));
    }
    return userObj;
  }

  public set user(userObj: UserModel) {
    if (userObj) {
      sessionStorage.setItem(this._sessionUserKey, JSON.stringify(userObj));
    } else {
      sessionStorage.removeItem(this._sessionUserKey);
    }
    this.UserChanged.next(userObj);
  }

  public get isLogged(): boolean {
    let retValue = false;
    const userObj: UserModel = this.user;
    retValue = (userObj && !this.tockenService.isTokenExpired()) ? true : false;
    // console.log('isLogged: ' + retValue);
    return retValue;
  }

  public get isAuthenticated(): boolean {
    return this.isLogged;
  }

  public get avatarUrl(): string {
    const retValue = '../assets/img/avatars/8.jpg';
    const userObj: UserModel = this.user;
    /*
    if (userObj) {
      const gravatarImgUrl = this.gravatar.url(userObj.eMail, 128, 'mm');
      if (gravatarImgUrl) {
        retValue = gravatarImgUrl;
      }
    }
    */
    return retValue;
  }

  //#endregion

  //#region Local

  //#region Language and Translation
  public LanguageChaged = new Subject<string>();

  public set language(lng: string) {
    if (lng) {
      const isChanged = this.language === lng;
      if (isChanged) {
        console.log('LanguageChaged->' + lng);
        localStorage.setItem(this._localLanguageKey, lng);

        // localStorage.removeItem(this._localLanguageLabelsKey);
        this.LanguageChaged.next(lng);
      }
    }
  }

  public get language(): string {

    let language = localStorage.getItem(this._localLanguageKey);
    if (!language) {
      language = 'en';
    }
    return language;

  }

  public get translations(): MlLabelsModel {
    const dataStr = localStorage.getItem(this._localLanguageLabelsKey);
    let dataObj: MlLabelsModel;

    if (dataStr) {
      dataObj = Object.assign(new MlLabelsModel(), JSON.parse(dataStr));
    }

    return dataObj;
  }

  public set translations(dataObj: MlLabelsModel) {
    if (dataObj) {
      localStorage.setItem(this._localLanguageLabelsKey, JSON.stringify(dataObj));
    }
  }


  //#endregion

  //#region Vehicles

  public get vehicles(): VehicleModel[] {
    const dataStr = localStorage.getItem(this._localVehiclesKey);
    let dataObj: VehicleModel[];

    if (dataStr) {

      dataObj = JSON.parse(dataStr);
    }

    return dataObj;
  }

  public set vehicles(dataObj: VehicleModel[]) {
    if (dataObj) {
      localStorage.setItem(this._localVehiclesKey, JSON.stringify(dataObj));
    }
  }

  public get selectedVehicle(): VehicleModel {
    const dataStr = localStorage.getItem(this._localSelectedVehicleKey);
    let dataObj: VehicleModel;

    if (dataStr) {

      dataObj = JSON.parse(dataStr);
    }

    return dataObj;
  }

  public set selectedVehicle(dataObj: VehicleModel) {
    if (dataObj) {
      localStorage.setItem(this._localSelectedVehicleKey, JSON.stringify(dataObj));
    }
  }


  //#region Statistics

  public vehiclesMaxDistancePassed(): number {
    let maxDistance = 0;

    if (this.vehicles) {
      this.vehicles.forEach(element => {
        if (maxDistance < (element.milage_km_current - element.milage_km_start)) {
          maxDistance = (element.milage_km_current - element.milage_km_start);
        }
      });
    }
    return maxDistance;
  }

  public vehiclesMaxTimeUsedMs(): number {
    let timeInUseMs = 0;

    if (this.vehicles) {
      this.vehicles.forEach((tmp: VehicleModel) => {
        if (timeInUseMs < tmp.timeInUseMs) {
          timeInUseMs = tmp.timeInUseMs;
        }
        /*
      if (tmp.purchase_date && tmp.purchase_date.date) {
        const purchase_date: Date = tmp.purchase_date.date instanceof Date ? tmp.purchase_date.date : new Date(tmp.purchase_date.date);
        timeInUseMs = new Date().getTime() - purchase_date.getTime();

        if (tmp.sale_date && tmp.sale_date.date) {
          const sale_date: Date = tmp.sale_date.date instanceof Date ? tmp.sale_date.date : new Date(tmp.sale_date.date);
          timeInUseMs = sale_date.getTime() - purchase_date.getTime();
        }
      }*/
      });
    }
    return timeInUseMs;
  }

  //#endregion

  //#endregion

  //#endregion

}
