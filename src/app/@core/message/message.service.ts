import { Injectable } from '@angular/core';
import { ToastsManager } from 'ng2-toastr';


@Injectable()
export class MessageService {

  messages: string[] = [];

  constructor(private toast: ToastsManager ) {
  }

  info(message: string, title: string) {
    this.messages.push(message);
    this.toast.info(message, title);
  }

  success(message: string, title: string) {
    this.messages.push(message);
    this.toast.success(message, title);
  }

  error(errObj: any, title: string) {
    // this.messages.push(message);
    this.toast.error(errObj, title);
  }


  clear() {
    this.messages = [];
  }

}
