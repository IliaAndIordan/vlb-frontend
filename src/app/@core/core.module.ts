import { NgModule, Optional, SkipSelf } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { NgModel } from '@angular/forms';
import { JWT_OPTIONS, JwtModule } from '@auth0/angular-jwt';
// Modules
// import { GravatarModule } from '@infinitycube/gravatar';

/**
 * Services
 */
import { throwIfAlreadyLoaded } from './module-import.gard';
// VLB Services
import { ValidationMessageService } from './validation/validation-message.service';
import { AuthService } from './auth/api/auth.service';
import { TokenService } from './auth/token.service';
import { TokenFactory } from './auth/token-factory';
import { MessageService } from './message/message.service';
import { SpinnerService } from './spinner/spinner.service';
import { CurrentUserService } from './auth/current-user.service';
import { AutGuard } from './guards/aut-guard.service';
import { JwtHelperService } from '@auth0/angular-jwt';
import { ApiHelper } from './api/api.helper';
import { VehicleService } from './api/vehicle/vehicle.service';
import { MlService } from './ml/api/ml.service';
import { ExpenseService } from './api/expenses/expense.service';
import { GasService } from './api/gas/gas.service';


@NgModule({
    imports: [
        HttpClientModule,
        /*GravatarModule,*/
        JwtModule.forRoot({
            jwtOptionsProvider: {
                provide: JWT_OPTIONS,
                useClass: TokenFactory
            }
        }),

    ],
    exports: [
        /*GravatarModule,*/
    ],
    providers: [
        ApiHelper,
        ValidationMessageService,
        MessageService,
        JwtHelperService,
        TokenService,
        AuthService,
        AutGuard,
        SpinnerService,
        CurrentUserService,
        VehicleService,
        MlService,
        ExpenseService,
        GasService,
    ]
})

export class CoreModule {
    constructor(@Optional() @SkipSelf() parentModule: CoreModule) {
        throwIfAlreadyLoaded(parentModule, 'CoreModule');
    }
}
