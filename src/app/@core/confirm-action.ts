export interface ConfirmAction {
    title: string;
    message: string;
    type: string;
}
