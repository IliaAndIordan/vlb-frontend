import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
// Modules
import { SharedModule } from '../@shared/shared.module';
import { FormSharedModule } from '../@shared/form_shared/form-shared.module';

// import { CoreModule } from '../@core/core.module';
 import { HomeRoutingModule, RoutedComponents } from './home-routing.module';



@NgModule({
  imports: [
    SharedModule,
    HomeRoutingModule,
    FormsModule, ReactiveFormsModule,
  ],
  declarations: [
    RoutedComponents
  ]
})
export class HomeModule { }
