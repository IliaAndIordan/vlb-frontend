import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
// import { HomeComponent } from './home.component';
import { LoginComponent } from './login/login.component';
import { AppRoutes } from '../@core/const/routes/app-routes.const';
import { HomeComponent } from './home.component';


export const routes: Routes = [
  { path: AppRoutes.Login, component: LoginComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class HomeRoutingModule { }
export const RoutedComponents = [LoginComponent, HomeComponent];

