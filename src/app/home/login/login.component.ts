import { Component, OnInit, OnDestroy, EventEmitter, Input, Output, ViewChild, ViewContainerRef } from '@angular/core';
import { Http } from '@angular/http';
import { ActivatedRoute, Router, Route, ActivatedRouteSnapshot, Params, RouterStateSnapshot } from '@angular/router';
import { FormBuilder, FormGroup, Validators, AbstractControl, ValidatorFn } from '@angular/forms';
import { Validator } from '../../@core/validation/validator';
import { HttpModule } from '@angular/http';
import 'rxjs/add/operator/debounceTime';
// Services
import { ToastsManager } from 'ng2-toastr/ng2-toastr';
import { ValidationMessageService } from '../../@core/validation/validation-message.service';

// Validators
import { UsernameValidator } from '../validators/username.validator';
import { PasswordValidator } from '../validators/password.validator';
// Components
// import { FormField } from '../../@shared/form_shared/inputs/form-field/form-field.component';

// Animation
import { Animate } from '../../@core/const/animation.const';
import { PageTransition, ShowHideTriggerBlock } from '../../animation';
import { AuthService } from '../../@core/auth/api/auth.service';
import { SpinnerService } from '../../@core/spinner/spinner.service';
import { TokenService } from '../../@core/auth/token.service';

// Model
import { UserModel } from '../../@core/auth/api/dtos';
import { AppRoutes } from '../../@core/const/routes/app-routes.const';
import { CurrentUserService } from '../../@core/auth/current-user.service';
import { TranslateService, TranslatePipe } from '@ngx-translate/core';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  animations: [PageTransition, ShowHideTriggerBlock]
})
export class LoginComponent implements OnInit {

  constructor(private toast: ToastsManager,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private httpModule: HttpModule,
    private fb: FormBuilder,
    private vms: ValidationMessageService,
    private autService: AuthService,
    private spinerService: SpinnerService,
    private tokenService: TokenService,
    private currentUserService: CurrentUserService) {
    // this.toast.setRootViewContainerRef(vcr);
  }

  @ViewChild('UserNameInput') userNameInput;

  private redirectTo: string = '';
  private state: string = Animate.in;
  formLogin: FormGroup;
  usernameValidator = new UsernameValidator(this.vms);
  passwordValidator = new PasswordValidator(this.vms);

  ngOnInit() {
    //  this.activatedRoute.queryParams
    //  .subscribe(params => this.redirectTo = params['redirectTo'] || '');
    // View Animation
    this.state = (this.state === Animate.in ? Animate.out : Animate.in);

    if (this.currentUserService.isAuthenticated) {
      const url = '/' + AppRoutes.UserProfile;
      this.router.navigate([AppRoutes.UserProfile]);
      return;
    }

    this.formLogin = this.fb.group({
      username: ['', this.usernameValidator.rules],
      password: ['', this.passwordValidator.rules]
    });

    // subscrive validators to value change in the form they livein
    this.usernameValidator.valueChangesSubscribe(this.formLogin);
    this.passwordValidator.valueChangesSubscribe(this.formLogin);

    this.userNameInput.nativeElement.focus();
    this.formLogin.updateValueAndValidity();
  }

  submit() {
    // Spinet Service
    this.spinerService.display(true);
    if (this.formLogin.valid) {
      this.autService.autenticate(
        this.usernameValidator.getValue(this.formLogin),
        this.passwordValidator.getValue(this.formLogin)
      )
        .subscribe(user => {
          if (user) {
            this.spinerService.display(false);
            this.toast.success('Welcome back ' + user.name + '!', 'Login success');
            setTimeout((router: Router) => {
              this.router.navigate(['/', AppRoutes.UserProfile]);
            }, 1000);
          } else {
            this.spinerService.display(false);
            this.toast.error('Login Failed', 'Login Failed');
          }
        });
    } else {
      this.toast.info('Please enter valid values for fields', 'Not valid input');
    }

  }

}
