// Type Definition File

declare module App {
    interface Service {
        name: string;
        url: string;
    }


    export interface Configuration {
        isDevelopment: boolean,
        isProduction: boolean,
        urls: Object;
        services: Object;
    }
}

declare var AppConfig: App.Configuration;